# hegira-components

## Installation

##### Configuration

After having downloaded the source code a new file should be created under the folder ``src/main/resources``:

* credentials.properties

In case one wants to configure hegira-components also for Apache Cassandra the file ``cassandraConfiguration.properties`` should be added to the same folder.

The structure of these files is shown below: 

###### credentials.properties
Contains the credentials that Hegira 4Clouds needs to access the databases. Currently supported databases are Google AppEngine Datastore, Microsoft Azure Tables and Apache Cassandra:

```java
azure.storageConnectionString=<escaped-azure-storage-connection-string>
zookeeper.connectString=<zookeeper-ip-address>:<port>
cassandra.server=<ip-address>
cassandra.username=<Cassandra-username>
cassandra.password=<Cassandra-password>
```

If configuring Google Datastore, notice that client login was [deprecated](https://developers.google.com/identity/protocols/AuthForInstalledApps#Errors). The preferred method to authenticate is thus Service Account Login (see the [wiki](https://bitbucket.org/hegira/hegira-components/wiki/Setup%20Datastore%20Remote%20api)).

In this case the configuration for the file **credentials.properties** is the following

```
#!java

datastore.SAclientId=<Service Account Client Id>
datastore.server=<application-name>.appspot.com
datastore.pkname=<certificate name as stored in the credentials.properties same folder>
```
Remember to place the certificate (named as declared in the property *datastore.pkname*) inside the same folder.

*Deprecated Client Login will be supported, for back porting reasons, until Google completely dismisses it*. To use add the following lines to the **credentials.properties** file:

```
#!java
datastore.username=<appengine-account-email-address>
datastore.password=<appengine-account-password>
datastore.server=<application-name>.appspot.com
datastore.login=client

```
Do not forget the property *datastore.login=client*, since this connection method is not selected by default any more!!!


###### cassandraConfiguration.properties
This file is located in the folder <home.user>/hegira/components. It contains the parameters that the user has to specify if the migration is from or to Cassandra. 

```java
cassandra.keyspace=<keyspace>
cassandra.readConsistency=<consistency-type>
cassandra.primarKey=<primary-key-column-name>
cassandra.readTimeOutMillis=<number> : default 60000
cassandra.connectTimeOutMillis=<number> : default 30000
```
The specified keyspace is the one in which data will be inserted.   
The consistency setting affects only read operations. The supported levels are: "eventual" and "strong".   
The primary key parameter specifies the name of the column that will be used as the primary key in all the tables to be migrated.  

###### hbaseConfiguration.properties
This file is located in the folder <home.user>/hegira/components. It contains the parameters that the user has to specify if the migration is from or to Apache HBase. 

```java
hbase.zookeeper.quorum=<zookeeper quorum string>
consistency.timeline=<true/false>
hegira.maxEntitiesInMem=<number>
rabbitmq.consumeIntervallMillis=<number>
```
The property *hegira.maxEntitiesInMem* refers to the maximum number of HBase entities that the migration system will load in memory and store in batch inside HBase; the greater the value, the higher the performance, but main memory can easily be exhausted if too big value is inserted. The default value, if not setted by the user, is 300 entities. The actual number of entities that will be stored in batch depends on the number of TWTs, in particular it will be equal to *hegira.maxEntitiesInMem/#TWTs*.

The property *rabbitmq.consumeIntervallMillis* refers to the time, in milliseconds, that each TWT will wait to read data from RabbitMQ before announcing that there is no data. By default its value is set to 5000ms plus a random time between 5s and 10s. The proper value to set depends on the deployment environment.

###### filter.properties
This file is located in the folder <home.user>/hegira/components. It contains the parameters that the user can optionally specify to blacklist or whitelist some data to be migrated from the source database. In particular, it accepts two keys (which can be combined, *exclusion* has the precedence, i.e., elements present in both lists will be excluded) **included** and **excluded**, followed by a comma-separated list of "table" to include or exclude respectively.


##### Build
The project is Maven compliant, hence by executing the command ```mvn clean package``` the proper packages will be created.

##### Deploy

In order to interact with Google AppEngine Datastore, an application should be deployed and run on Google AppEngine; in particular, the application web.xml file should allow for `Remote Api` to be accepted by the Google AppEngine application, as described [here](https://cloud.google.com/appengine/docs/java/tools/remoteapi)

## Usage: 
Once compiled, a jar file (with dependencies) is generated inside the target/ folder.
To execute one of the components, issue the following command:
```java
java -jar hegira-components.jar [options] [command] [command options]
```
  Options:
  
    --queue, -q
    
       RabbitMQ queue address.
       
       Default: localhost
       
    --type, -t
   
       launch a SRC or a TWC

```

Commands:
    tests      Executes the component in test mode, allowing the user to provide additional parameters
      Usage: tests [options]
        Options:
          --eName, -e
             A name should be given to the statistics.
             Default: exp1473864024542
          --iDelay, -i
             The initial time to delay sampling execution.
             Default: 1
          --sDelay, -d
             The time from now to delay execution of the task that will stop the
             component's threads (SRTs or TWTs).
             Default: -1
          --samPer, -p
             The period between successive samples.
             Default: -1
          --unit, -u
             The time unit of the delay parameter.
             Default: SECONDS
          --unitSam, -us
             The time unit of both the init-delay and sample-period parameters.
             Default: MINUTES

```

##License##

Licensed under the [Apache License, Version 2.0][1]

[1]: http://www.apache.org/licenses/LICENSE-2.0