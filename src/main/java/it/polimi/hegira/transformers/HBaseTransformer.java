/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.transformers;

import it.polimi.hegira.models.Column;
import it.polimi.hegira.models.HBaseModel;
import it.polimi.hegira.models.HBaseModel.HBaseCell;
import it.polimi.hegira.models.HBaseModel.HBaseColumn;
import it.polimi.hegira.models.HBaseModel.HBaseRow;
import it.polimi.hegira.models.Metamodel;
import it.polimi.hegira.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.hbase.client.Consistency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HBaseTransformer implements ITransformer<HBaseModel> {
	
	private org.apache.hadoop.hbase.client.Consistency consistencyType = org.apache.hadoop.hbase.client.Consistency.STRONG;
	private static final Logger log = LoggerFactory.getLogger(HBaseTransformer.class);
	
	public HBaseTransformer() {
		super();
	}
	
	public HBaseTransformer(org.apache.hadoop.hbase.client.Consistency consistencyType) {
		this.consistencyType = consistencyType;
	}

	@Override
	public Metamodel toMyModel(HBaseModel model) {
		Metamodel res = new Metamodel();
		
		List<String> columnFamilies = new ArrayList<>();
		
		for (HBaseCell cell : model.getCells()) {
			log.debug("Trasforming {}", cell.toString());
			
			res.setRowKey(cell.getRowKey());
			
			res.setPartitionGroup(getPartitionKey(cell));
			
			String[] columnNameParts = cell.getColumnKey().split(":");
			if (columnNameParts.length != 2)
				continue;
			
			String columnFamily = columnNameParts[0];
			List<Column> cs = res.getColumns().get(columnFamily);
			if (cs == null) {
				cs = new ArrayList<Column>();
				res.getColumns().put(columnFamily, cs);
				columnFamilies.add(columnFamily);
			}
			
			Column actualColumn = null;
			
			for (int i = 0; i < cs.size() && actualColumn != null; ++i) {
				Column c = cs.get(i);
						
				if (c.getColumnName().equals(columnNameParts[1]))
					actualColumn = c;
			}
			if (actualColumn == null) {
				actualColumn = new Column();
				actualColumn.setColumnName(columnNameParts[1]);
				cs.add(actualColumn);
			}
			
			actualColumn.setColumnValue(cell.getValue());
			actualColumn.setColumnValueType("byte[]");
			actualColumn.setTimestamp(DateUtils.format(cell.getTimestamp().getTime()));
		}
		
		res.setColumnFamilies(columnFamilies);
		
		return res;
	}

	@Override
	public HBaseModel fromMyModel(Metamodel model) {
		String tableKey = extractTableName(model.getPartitionGroup());
		String rowKey = model.getRowKey();
		
		HBaseModel res = new HBaseModel(tableKey);
		HBaseRow row = res.addRow(rowKey);
		
		Map<String, List<Column>> columnsMap = model.getColumns();
		
		for (String family : model.getColumnFamilies()) {
			List<Column> columns = columnsMap.get(family);
			for (Column c : columns) {
				String fullColumnName = String.format("%s:%s", family, c.getColumnName());
				
				HBaseColumn column = row.getColumns().get(fullColumnName);
				if (column == null)
					column = row.addColumn(family, c.getColumnName());
				
				column.addCell(c.getColumnValue(), c.getTimestamp() != null ? DateUtils.parse(c.getTimestamp()) : null);
			}
		}
		
		return res;
	}
	
	private String extractTableName(String partitionGroup){
		/*
		 * Datastore should have removed the @, but anyway...
		 */
		partitionGroup = partitionGroup.replaceAll("@", "");
		
		int lastIndexOf = partitionGroup.lastIndexOf("#");
		if(lastIndexOf > -1){
			return partitionGroup.substring(0, lastIndexOf);
		}
		return null;
	}
	
	@SuppressWarnings("unused")
	private String extractPartitionKey(String partitionGroup){
		int lastIndexOf = partitionGroup.lastIndexOf("#");
		if(lastIndexOf > -1){
			return partitionGroup.substring(lastIndexOf+1);
		}
		return null;
	}
	
	private String getPartitionKey(HBaseCell cell) {
		if (consistencyType == Consistency.STRONG) {
			return String.format("@%s#1", cell.getTableKey());
		} else {
			return String.format("@%s#%s", cell.getTableKey(), cell.getRowKey());
		}
	}

}
