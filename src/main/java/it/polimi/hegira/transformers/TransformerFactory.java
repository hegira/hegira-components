/**
 * 
 */
package it.polimi.hegira.transformers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class TransformerFactory {
	private static Logger log = LoggerFactory.getLogger(TransformerFactory.class);
	public enum Transformers {
		TDatastore, TTables, TCassandra, THBase
	}
	
	public static ITransformer getTransformer(Transformers transformer){
		switch(transformer){
		case TDatastore:
			return new DatastoreTransformer();
		case TTables:
			return new AzureTablesTransformer();
		case TCassandra:
			return new CassandraTransformer();
		case THBase:
			return new HBaseTransformer();
		default:
			log.error("{} - Transformer {} not supported",
					Thread.currentThread().getName(),
					transformer.name());
		}
		
		return null;
	}
}
