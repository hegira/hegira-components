/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.adapters;

import it.polimi.hegira.coordination.VDPsCheckerController;
import it.polimi.hegira.coordination.ZooKeeper;
import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.exceptions.FailedLockException;
import it.polimi.hegira.exceptions.QueueException;
import it.polimi.hegira.models.Metamodel;
import it.polimi.hegira.queue.TaskQueue;
import it.polimi.hegira.queue.VDPsQueue;
import it.polimi.hegira.queue.events.MigratedVdpEvent;
import it.polimi.hegira.queue.events.MigratedVdpListener;
import it.polimi.hegira.queue.events.MigratedVdpSource;
import it.polimi.hegira.queue.events.PauseSource;
import it.polimi.hegira.queue.events.SlowDownSource;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.PropertiesManager;
import it.polimi.hegira.utils.VDPsCounters;
import it.polimi.hegira.vdp.VdpUtils;
import it.polimi.hegira.zkWrapper.MigrationStatus;
import it.polimi.hegira.zkWrapper.MigrationStatus.VDPstatus;
import it.polimi.hegira.zkWrapper.VDPmigrationStatus;
import it.polimi.hegira.zkWrapper.ZKclient;
import it.polimi.hegira.zkWrapper.ZKcomponentStatus;
import it.polimi.hegira.zkWrapper.ZKserver;
import it.polimi.hegira.zkWrapper.ZKserver2;
import it.polimi.hegira.zkWrapper.statemachine.MigrationComponentStatus;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDatabase implements Runnable, MigratedVdpListener{
	//protected TaskQueue taskQueue;
	protected ArrayList<TaskQueue> taskQueues;
	private transient Logger log = LoggerFactory.getLogger(AbstractDatabase.class);
	protected int TWTs_NO = 0, SRTs_NO = 1;
	
	//String tableName
	//MigrationStatus the migration status for that table
	protected ConcurrentHashMap<String, MigrationStatus> snapshot;
	protected ConcurrentHashMap<String, VDPmigrationStatus> snapshot2;
	protected VDPsQueue<Integer> vdpsQueue;
	protected int vdpSize;
	String connectString = PropertiesManager.getZooKeeperConnectString();
	//ugly but for prototyping...
	//(normal || partitioned)
	private String behavior = "normal"; 
	
	/**
	 * VDPsCounters
	 * K1 tableName
	 * V1-K2 VDPid
	 * V1-V2 VDP counter 
	 */
	protected VDPsCounters vdpsCounters;
	
	/**
	 * Source object to send/stop pause and slowdown requests
	 */
	private PauseSource pauseSource;
	private SlowDownSource slowdownSource;
	
	protected boolean stopThreads = false; 
	
	private ZKserver zkServer = null;
	private Object zkserver_lock = new Object();
	protected boolean useZkServer2 = false;
	private ZKserver2 zkServer2 = null;
	private Object zkserver2_lock = new Object();
	private ZKclient zkClient = null;
	private Object zkclient_lock = new Object();
	
	private ZKcomponentStatus zkStatuses = null;
	
	private boolean isMigrationFinished=false;
	
	private ConcurrentHashMap<String, Integer> vdpsTables;
	private ConcurrentHashMap<String, Integer> vdpsTablesCopy;
	private VDPsCheckerController vdPsCheckerController;
	private CopyOnWriteArrayList<String> migratedTablesList;
	
	/**
	* Constructs a general database object
	* @param options A map containing the properties of the new db object to be created.
	* 			<code>mode</code> Consumer or Producer.
	* 			<code>threads</code> The number of consumer threads.
	* 			<code>queue-address</code>
	*/
	protected AbstractDatabase(Map<String, String> options){
		slowdownSource = SlowDownSource.getInstance();
		pauseSource = PauseSource.getInstance();
		if(options==null){
			log.error("{} - Not instantiated correctly options cannot be Null, unless under tests",
					Thread.currentThread().getName());
			return;
		}
		
		if(options.containsKey("zkServer.version")){
			try{
				int version = Integer.parseInt(options.get("zkServer.version"));
				if(version>1)
					useZkServer2=true;
			}catch(Exception e){}
		}
		
		try{
			if(connectString!=null){
				zkStatuses = new ZKcomponentStatus(connectString);
				if(zkStatuses!=null){
					MigrationComponentStatus status = null;
					if(options.get("mode").equals(Constants.PRODUCER)){
						status = retrieveComponentStatus("SRC");
					}else if(options.get("mode").equals(Constants.CONSUMER)){
						status = retrieveComponentStatus("TWC");
					}
					status.setActive();
					zkStatuses.setMigrationComponentStatus(status);
				}
			}else{
				log.warn("{} - Hegira was not configured to run with Zookeeper. Components statuses won't be persisted",
						Thread.currentThread().getName());
			}
		} catch (KeeperException | InterruptedException e1) {
			log.warn("{} - Hegira was not configured to run with Zookeeper. Components statuses won't be persisted. ",
					Thread.currentThread().getName(),e1);
		}
		
		try {
			switch(options.get("mode")){
				case Constants.PRODUCER:
					if(!useZkServer2)
						snapshot = new ConcurrentHashMap<String, MigrationStatus>();
					else
						snapshot2 = new ConcurrentHashMap<String, VDPmigrationStatus>();
					vdpsQueue = new VDPsQueue<Integer>();
					
					int srts_no = 1;
					if(options.get("SRTs_NO")!=null){
						int parseInt = Integer.parseInt(options.get("SRTs_NO"));
						srts_no = parseInt<1 ? 1 : parseInt;
					}
					this.SRTs_NO = srts_no;
					
					taskQueues=new ArrayList<TaskQueue>(srts_no);
					for(int i=0;i<srts_no;i++){
						TaskQueue tqp = new TaskQueue(options.get("mode"), 0, 
								options.get("queue-address"));
						tqp.setZkComponentsStatuses(zkStatuses);
						taskQueues.add(tqp);
					}
					
					break;
				case Constants.CONSUMER:
					int threads=10;
					if(options.get("threads")!=null)
						threads = Integer.parseInt(options.get("threads"));
					
					taskQueues=new ArrayList<TaskQueue>(threads);

					this.TWTs_NO=threads;
					for(int i=0;i<threads;i++){
						TaskQueue tqc = new TaskQueue(options.get("mode"), 
								Integer.parseInt(options.get("threads")), 
								options.get("queue-address"));
						tqc.setZkComponentsStatuses(zkStatuses);
						taskQueues.add(tqc);
					}
					
					vdpsCounters = new VDPsCounters();
					break;
				default:
					log.error(Thread.currentThread().getName()+
							"Unsuported mode: "+options.get("mode"));
					return;
			}
			
		} catch (NumberFormatException | QueueException e) {
			e.printStackTrace();
			return;
		}
	}
	
	
	private void loadVDPchecker(){
		vdpsTables = new ConcurrentHashMap<String, Integer>();
		migratedTablesList = new CopyOnWriteArrayList<String>();
		try {
			//1. Retrieve the total VDP numbers in the main process
			@SuppressWarnings("static-access")
			ZooKeeper zk = ZooKeeper.init(PropertiesManager.getZooKeeperConnectString()).getInstance();
			boolean isPopulated = false;
			do {
				Thread.sleep(1000);
				isPopulated = populateVdpsTables(zk);
				log.error("Does Zookeeper contain the VDP statuses? Please check.");
			} while(!isPopulated);
			
			//2. Create a thread that periodically checks for the last VDPs statuses and sends a notification
			vdPsCheckerController = new VDPsCheckerController(zk, vdpsTables);
			vdPsCheckerController.startThread();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	* Encapsulate the logic contained inside the models to map to the intermediate model
	* to a DB
	* @param mm The intermediate model
	* @return returns the converted model
	*/
	protected abstract AbstractDatabase fromMyModel(Metamodel mm);
	
	/**
	* Encapsulate the logic contained inside the models to map to the intermediate model
	* to a DB in a partitioned way.
	* @param mm The intermediate model
	* @return returns the converted model
	*/
	protected abstract AbstractDatabase fromMyModelPartitioned(Metamodel mm);
	
	/**
	* Encapsulate the logic contained inside the models to map a DB to the intermediate
	* model
	* @param model The model to be converted
	* @return returns The intermediate model
	*/
	protected abstract Metamodel toMyModel(AbstractDatabase model);
	/**
	 * Start a connection towards the database. 
	 * Suggestion: To be called inside a try-finally block. Should always be disconnected
	 * @throws ConnectException
	 */
	public abstract void connect() throws ConnectException;
	/**
	 * Disconnects and closes all connections to the database.
	* Suggestion: To be called inside a finally block
	*/
	public abstract void disconnect();
	
	/**
	 * Encapsulate the logic contained inside the models to map a DB to the intermediate
	 * model, reading from the source database in a virtually partitioned way
	 * @param model
	 * @return
	 */
	protected abstract Metamodel toMyModelPartitioned(AbstractDatabase model);
	
	/**
	 * Template method which performs the non-partitioned data migration.
	 * @param component A string representing the component, i.e. SRC or TWC
	 * @return true if the migration has completed successfully from the point of view of the component who called it.
	 */
	public final boolean switchOver(final String component){
		final AbstractDatabase thiz = this;
		behavior = "normal";
		
		if(component.equals("SRC")){
			(new Thread() {
				@Override public void run() {
					int thread_id = (int) ((SRTs_NO<=1) ? 0 : Thread.currentThread().getId()%SRTs_NO);
					
					//making the task queues listen for pause events
					pauseSource.addPauseListener(taskQueues.get(thread_id));
					log.debug(Thread.currentThread().getName()+
							" - Registering pause listener on task queue element no: "+thread_id);
					
					//making the task queues listen for slowdown events
					slowdownSource.addSlowDownListener(taskQueues.get(thread_id));
					log.debug(Thread.currentThread().getName()+
							" - Registering slowdown listener on task queue element no: "+thread_id);
					
					try {
						thiz.connect();
						storeStartMigration(component);
						thiz.toMyModel(thiz);
						storedFinishedMigration(component);
					} catch (ConnectException e) {
						storeInactiveComponent(component);
						e.printStackTrace();
					} finally{
						thiz.disconnect();
					}     
				}
			}).start();
		}else if(component.equals("TWC")){
			//executing the consumers
			ExecutorService executor = Executors.newFixedThreadPool(thiz.TWTs_NO);
			log.debug("EXECUTOR switchover No. Consumer threads: "+thiz.TWTs_NO);
			for(int i=0;i<thiz.TWTs_NO;i++){
				executor.execute(thiz);
			}
		}
		return true;
	}
	
	private void storeStartMigration(String component){
		if(zkStatuses!=null){
			MigrationComponentStatus status = retrieveComponentStatus(component);
			status.startMigration();
			zkStatuses.setMigrationComponentStatus(status);
		}
	}
	
	private void storeInactiveComponent(String component){
		if(zkStatuses!=null){
			MigrationComponentStatus status = retrieveComponentStatus(component);
			status.setInactive();
			zkStatuses.setMigrationComponentStatus(status);
		}
	}
	
	private void storedFinishedMigration(String component){
		if(zkStatuses!=null){
			MigrationComponentStatus status = retrieveComponentStatus(component);
			status.finishMigration();
			zkStatuses.setMigrationComponentStatus(status);
		}
	}
	
	private Collection<String> filterTableList(Collection<String> fullList){
		   List<String> excluded = PropertiesManager.getExcludedTables();
		   List<String> included = PropertiesManager.getIncludedTables();
		   
		   int fl_size = fullList.size();
		   int ex_size = excluded.size();
		   int inc_size = included.size();
		   
		   if(excluded.isEmpty() && included.isEmpty()){
			   return fullList;
		   }
		   
		   if(excluded!=null && !excluded.isEmpty() && included != null && !included.isEmpty()){
			   //removing elements from the exclusion list that should be explicitly allowed
			   excluded.removeAll(included);
		   }
		   
		   Collection<String> newList = new ArrayList<String>(fullList);
		   
		   if(excluded != null && !excluded.isEmpty()){
			   //blacklisting
			   newList.remove(excluded);
		   }
		   
		   if(included != null && !included.isEmpty()){
			   //whitelisting
			   newList.retainAll(included);
		   }
		   
		   log.debug("{} - filtering snapshot. Full tables list size: {}. "
		   		+ "Exclusion list size: {}. Inclusion list size: {}. "
		   		+ "Final list size: {}",
				   Thread.currentThread().getName(),
				   fl_size, ex_size, inc_size, newList.size());
		   
		   return newList;
	   }
	
	/**
	 * Template method
	 * Performs the switchOver in a virtually partitioned way.
	 * The TWC part is unchanged with respect to the switchOver method.
	 * @param component The name of the component (i.e., SRC or TWC)
	 * @param recover <b>true</b> if the migration should be recovered from the last successfully migrated VDP; <b>false</b> otherwise
	 * @return
	 */
	public final boolean switchOverPartitioned(String component, final boolean recover){
		final AbstractDatabase thiz = this;
		behavior = "partitioned";
		try {
			if(!useZkServer2)
				vdpSize = getZkServer().getVDPsize();
			else
				vdpSize = getZkServer2().getVDPsize();
			log.debug("Got VDPsize = "+vdpSize);
		} catch (Exception e) {
			log.error("Unable to retrieve VDP size", e);
		}
		
		if(component.equals("SRC")){
			//Creating the snapshot
			try {
				thiz.connect();
				storeStartMigration(component);
				if(!recover){
					log.debug(Thread.currentThread().getName()+
							" creating snapshot...");
					thiz.createSnapshot((List<String>) filterTableList(thiz.getTableList()));
				}else{
					log.debug(Thread.currentThread().getName()+
							" recoverying snapshot...");
					thiz.restoreSnapshot((List<String>) filterTableList(thiz.getTableList()));
				}
			} catch (ConnectException e) {
				storeInactiveComponent(component);
				e.printStackTrace();
			} catch (Exception e) {
				storeInactiveComponent(component);
				e.printStackTrace();
			} finally{
				thiz.disconnect();
			} 
			
			//parallel data extraction
			for(int i=0; i<SRTs_NO;i++){
				(new Thread() {
					@Override public void run() {
						
						int thread_id = (int) (Thread.currentThread().getId()%SRTs_NO);
						
						//making the task queues listen for pause events
						pauseSource.addPauseListener(taskQueues.get(thread_id));
						log.debug(Thread.currentThread().getName()+
								" - Registering pause listener on task queue element no: "+thread_id);
						
						//making the task queues listen for slowdown events
						slowdownSource.addSlowDownListener(taskQueues.get(thread_id));
						log.debug(Thread.currentThread().getName()+
								" - Registering slowdown listener on task queue element no: "+thread_id);
						
						
						try {
							thiz.connect();
							thiz.toMyModelPartitioned(thiz);
						} catch (ConnectException e) {
							e.printStackTrace();
						} catch (Exception e) {
							e.printStackTrace();
						} finally{
							thiz.disconnect();
						}     
					}
				}).start();
				
			}
		}else if(component.equals("TWC")){
			//executing the consumers
			ExecutorService executor = Executors.newFixedThreadPool(thiz.TWTs_NO,
					new TWTFactory());
			log.debug("EXECUTOR switchover No. Consumer threads: "+thiz.TWTs_NO);
			storeStartMigration("TWC");
			loadVDPchecker();
			for(int i=0;i<thiz.TWTs_NO;i++){
				executor.execute(thiz);
			}
			
			MigratedVdpSource.getInstance().addMigratedVdpListener(thiz);
			log.debug(Thread.currentThread().getName()+
					" - Registering migration finished listener");
		}
		return true;
	}
	
	@Override
	public void run() {
		int thread_id = (int) ((TWTs_NO==0) ? 0 : Thread.currentThread().getId()%TWTs_NO);
		
		//making the task queues listen for pause events
		pauseSource.addPauseListener(taskQueues.get(thread_id));
		log.debug(Thread.currentThread().getName()+
				" - Registering pause listener on task queue element no: "+thread_id);
		
		//making the task queues listen for slowdown events
		slowdownSource.addSlowDownListener(taskQueues.get(thread_id));
		log.debug(Thread.currentThread().getName()+
				" - Registering slowdown listener on task queue element no: "+thread_id);
		
		try {
			this.connect();
			log.debug(Thread.currentThread().getName()+" - Starting consumer thread");
			if(behavior.equals("normal"))
				this.fromMyModel(null);
			else if(behavior.equals("partitioned")){
				this.fromMyModelPartitioned(null);
			}
		} catch (ConnectException e) {
			log.error(Thread.currentThread().getName() +
					" Unable to connect to the destination database!", e);
		}	
	}
	
	private void enqueueVDPs(String tableName, Integer totalVDPs){
		Integer i;
		int failed=0;
		for(i=0;i<totalVDPs;i++){
			boolean added = vdpsQueue.add(tableName, i);
			if(!added)
				failed++;
		}
		
		log.debug("{} - Table {}. Enqueued {} vdps. Failed inserts: {}",Thread.currentThread().getName(),
				tableName, i-failed, failed);
	}

	/**
	 * Creates a new snapshot of the source database by considering just 
	 * the list of tables given as input.
	 * Notice that the previous snapshot data, on ZooKeeper, will be deleted.
	 * @param tablesList The list of tables that will be considered to create the snapshot.
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	protected void createSnapshot(List<String> tablesList) throws Exception{
		//set the queryLock used as a flag to block query propagation 
		//until the snapshot has been completely created.
		if(!useZkServer2)
			getZkServer().lockQueries();
		else
			getZkServer2().lockQueries();
		log.debug(Thread.currentThread().getName()+
				" locking queries...");
		
		try {
			//getting the VDPsize
			//vdpSize = zKserver.getVDPsize();
			for(String tbl : tablesList){
				int seqNr = getZkClient().getCurrentSeqNr(tbl);
				int totalVDPs = VdpUtils.getTotalVDPs(seqNr, vdpSize);
				//automatically setting the VDPStatus to NOT_MIGRATED
				if(!useZkServer2){
					MigrationStatus status = new MigrationStatus(seqNr, totalVDPs);
					boolean setted = getZkServer().setFreshMigrationStatus(tbl, status);
					snapshot.put(tbl, status);
				}else{
					for(int vdpId=0; vdpId<totalVDPs; vdpId++){
						VDPmigrationStatus status = new VDPmigrationStatus(vdpId, seqNr, totalVDPs);
						boolean setted = getZkServer2().setFreshMigrationStatus(tbl, vdpId+"", status);
						snapshot2.put(tbl+vdpId, status);
					}
				}
				
				log.debug(Thread.currentThread().getName()+
						" Added table "+tbl+" to snapshot with seqNr: "+seqNr+" and totalVDPs: "+totalVDPs);
				enqueueVDPs(tbl, totalVDPs);
			}
		} catch (Exception e) {
			log.error(Thread.currentThread().getName() +
					" Unable to create a snapshot!", e);
		}
		
		//release the queryLock
		if(!useZkServer2)
			getZkServer().unlockQueries();
		else
			getZkServer2().unlockQueries();
		log.debug(Thread.currentThread().getName()+
				" unlocked queries...");
		log.debug("{} - VDPsQueue Content: {}",
				Thread.currentThread().getName(),
				vdpsQueue.toString());
		disconnectZkClient();
	}
	
	/**
	 * Restores the snapshot considering the MigrationStatus stored in ZooKeeper.
	 * @param tablesList The list of tables that will be considered to create the snapshot.
	 * @throws Exception ZK errors, interruptions, etc.
	 */
	@SuppressWarnings("deprecation")
	protected boolean restoreSnapshot(List<String> tableList) throws FailedLockException, Exception{
		boolean returnValue =  false;
		if(!useZkServer2)
			getZkServer().lockQueries();
		else
			getZkServer2().lockQueries();
		
		for(String tbl : tableList){
			try{
				if(!useZkServer2)
					while(!getZkServer().acquireLock(tbl)){
						log.error(Thread.currentThread().getName()+
								" Cannot acquire lock on table: "+tbl+". Retrying!");
					}
				else
					while(!getZkServer2().acquireFullTableLock(tbl)){
						log.error(Thread.currentThread().getName()+
								" Cannot acquire lock on table: "+tbl+". Retrying!");
					}
			}catch(Exception e){
				//failed to acquire the lock
				log.error("{} - ZK error when trying to acquire lock! Retrying",
						Thread.currentThread().getName());
				returnValue=restoreSnapshot(tableList);
				if(returnValue) return true;
			}
			
			try{
				if(!useZkServer2){
					MigrationStatus migrationStatus = getZkServer().getFreshMigrationStatus(tbl, null);
					//reverting all VDPs which are UNDER_MIGRATION to NOT_MIGRATED
					HashMap<Integer, StateMachine> vdps = migrationStatus.getVDPs();
					Set<Integer> keys = vdps.keySet();
					for(Integer vdpId : keys){
						StateMachine sm = vdps.get(vdpId);
						State currentState = sm.getCurrentState();
						if(currentState.equals(State.UNDER_MIGRATION)){
							migrationStatus.getVDPs().put(vdpId, new StateMachine());
							getZkServer().setFreshMigrationStatus(tbl, migrationStatus);
							log.debug(Thread.currentThread().getName()+
									" reverting "+tbl+"/VDP "+vdpId+" to NOT_MIGRATED");
							vdpsQueue.add(tbl, vdpId);
						}else if(currentState.equals(State.NOT_MIGRATED)){
							vdpsQueue.add(tbl, vdpId);
						}
						//TODO: revert SYNC states to NOT_MIGRATED?
					}
					
					getZkServer().releaseLock(tbl);
					snapshot.put(tbl, migrationStatus);
				}else{
					HashMap<Integer, StateMachine> vdps = getZkServer2().getFreshVDPs(tbl);
					Set<Integer> keys = vdps.keySet();
					for(Integer vdpId : keys){
						StateMachine sm = vdps.get(vdpId);
						State currentState = sm.getCurrentState();
						if(currentState.equals(State.UNDER_MIGRATION)){
							VDPmigrationStatus ms = getZkServer2().getMigrationStatus(tbl, vdpId+"");
							ms.resetStateMachine();
							getZkServer2().setFreshMigrationStatus(tbl, vdpId+"", ms);
							log.debug(Thread.currentThread().getName()+
									" reverting "+tbl+"/VDP "+vdpId+" to NOT_MIGRATED");
							vdpsQueue.add(tbl, vdpId);
						}else if(currentState.equals(State.NOT_MIGRATED)){
							vdpsQueue.add(tbl, vdpId);
						}
					}
				}
				log.debug(Thread.currentThread().getName()+
						" Added table "+tbl+" to snapshot");
			} catch (Exception e){
				if(!useZkServer2)
					getZkServer().releaseLock(tbl);
				else
					getZkServer2().releaseFullTableLock(tbl);
				throw new Exception();
			}
		}
		
		if(!useZkServer2)
			getZkServer().unlockQueries();
		else
			getZkServer2().unlockQueries();
		return returnValue=true;
	}
	
	private boolean acquireBlockingLock(String tableName) throws FailedLockException {
		boolean acquired = false;
		if(getZkServer() == null || tableName == null) return acquired;
		
		try{
			while(!getZkServer().acquireLock(tableName)){
				//log.error(Thread.currentThread().getName()+
				//			" Cannot acquire lock on table: "+tableName+". Retrying!");
			}
			acquired = true;
		}catch(Exception e){
			//failed to acquire the lock
			throw new FailedLockException();
		}
		
		return acquired;
	}
	
	private boolean acquireBlockingLock(String tableName, int VDPid) throws FailedLockException {
		boolean acquired = false;
		if(getZkServer2() == null || tableName == null) return acquired;
		
		try{
			while(!getZkServer2().acquireLock(tableName, VDPid+"")){
				//log.error(Thread.currentThread().getName()+
				//			" Cannot acquire lock on table: "+tableName+". Retrying!");
			}
			acquired = true;
		}catch(Exception e){
			//failed to acquire the lock
			throw new FailedLockException();
		}
		
		return acquired;
	}
	
	/**
	 * Tells the SRC if it is allowed to migrate a given VDP for a given table.
	 * @param tableName The name of the table to migrate.
	 * @param VDPid The id of the VDP to be migrated.
	 * @return true if it is possible to migrate the given VDP, false otherwise.
	 * @throws FailedLockException If the function was not able to acquire the lock due to a ZK error.
	 * @throws Exception ZooKeeper exception
	 */
	public boolean canMigrate(String tableName, int VDPid) throws FailedLockException, Exception {
		if(!useZkServer2)
			acquireBlockingLock(tableName);
		else
			acquireBlockingLock(tableName, VDPid);
		
		try{
			MigrationStatus migStatus=null;
			VDPmigrationStatus vdpMigStatus=null;
			State currentState;
			if(!useZkServer2){
				migStatus = getZkServer().getFreshMigrationStatus(tableName, null);
				currentState = migStatus.getVDPstatus(VDPid).getCurrentState();
			}else{
				vdpMigStatus = getZkServer2().getFreshMigrationStatus(tableName, VDPid+"", null);
				currentState = vdpMigStatus.getVDPstatus().getCurrentState();
			}
			
			if(currentState.equals(State.MIGRATED)){
				log.debug(Thread.currentThread().getName() +
						" migration status already "+currentState.name()+" for VDP: "+tableName+"/"+VDPid);
				if(!useZkServer2){
					snapshot.put(tableName, migStatus);
					getZkServer().releaseLock(tableName);
				}else{
					snapshot2.put(tableName+VDPid, vdpMigStatus);
					getZkServer2().releaseLock(tableName, VDPid+"");
				}
				return false;
			}else if(currentState.equals(State.SYNC)){
				if(!useZkServer2){
					snapshot.put(tableName, migStatus);
					getZkServer().releaseLock(tableName);
				}else{
					snapshot2.put(tableName+VDPid, vdpMigStatus);
					getZkServer2().releaseLock(tableName, VDPid+"");
				}
				do{
					log.debug(Thread.currentThread().getName()+
							" Looping until the state changes back to NOT_MIGRATED");
					Thread.sleep(300);
					
					if(!useZkServer2){
						migStatus = getZkServer().getFreshMigrationStatus(tableName, null);
						currentState = migStatus.getVDPstatus(VDPid).getCurrentState();
					}else{
						vdpMigStatus = getZkServer2().getFreshMigrationStatus(tableName, VDPid+"", null);
						currentState = vdpMigStatus.getVDPstatus().getCurrentState();
					}
				}while(currentState.equals(State.SYNC));
				
				if(!useZkServer2){
					acquireBlockingLock(tableName);
					snapshot.put(tableName, migStatus);
				}else{
					acquireBlockingLock(tableName, VDPid);
					snapshot2.put(tableName+VDPid, vdpMigStatus);
				}
				
			} else if(currentState.equals(State.UNDER_MIGRATION)){
				if(!useZkServer2){
					snapshot.put(tableName, migStatus);
					getZkServer().releaseLock(tableName);
				}else{
					snapshot2.put(tableName+VDPid, vdpMigStatus);
					getZkServer2().releaseLock(tableName, VDPid+"");
				}
				return false;
			}
			
			VDPstatus migrateVDP = !useZkServer2 ? migStatus.migrateVDP(VDPid) : vdpMigStatus.migrateVDP();
			
			int retries = 0;
			if(!useZkServer2){
				boolean setted = false;
				while(!setted && retries < 10){
					setted = getZkServer().setFreshMigrationStatus(tableName, migStatus);
					if(!setted){
						log.error(Thread.currentThread().getName() + 
								" ==> !!! Couldn't update the migration status to "+migrateVDP.name()+" for VDP: "+tableName+"/"+VDPid);
						Thread.sleep(100);
						retries++;
					}
				}
				snapshot.put(tableName, migStatus);
				getZkServer().releaseLock(tableName);
			}else{
				boolean setted = false;
				while(!setted && retries<10){
					setted = getZkServer2().setFreshMigrationStatus(tableName, VDPid+"", vdpMigStatus);
					if(!setted){
						log.error(Thread.currentThread().getName() + 
							" ==> !!! Couldn't update the migration status to "+migrateVDP.name()+" for VDP: "+tableName+"/"+VDPid);
						Thread.sleep(100);
						retries++;
					}
				}
				snapshot2.put(tableName+VDPid, vdpMigStatus);
				getZkServer2().releaseLock(tableName, VDPid+"");
			}
			
			if(!useZkServer2){
				log.debug(Thread.currentThread().getName() +
					" updated migration status to "+migStatus.getVDPstatus(VDPid).getCurrentState()+" for VDP: "+tableName+"/"+VDPid);
				return migStatus.getVDPstatus(VDPid).getCurrentState().name().equals(VDPstatus.UNDER_MIGRATION.name()) && retries < 10 ? true : false;
			}else{
				log.debug(Thread.currentThread().getName() +
						" updated migration status to "+vdpMigStatus.getVDPstatus().getCurrentState()+" for VDP: "+tableName+"/"+VDPid);
				return vdpMigStatus.getVDPstatus().getCurrentState().name().equals(VDPstatus.UNDER_MIGRATION.name()) && retries < 10 ? true : false;
			}
		} catch (Exception e){
			if(!useZkServer2)
				getZkServer().releaseLock(tableName);
			else
				getZkServer2().releaseLock(tableName, VDPid+"");
			
			throw new Exception();
		}
	}
	
	/**
	 * Called from the TWC to announce the complete migration of a given VDP.
	 * @param tableName The table name.
	 * @param VDPid The id of the VDP the TWC has finished to migrate.
	 * @return true if the operation succeeded, false otherwise.
	 * @throws FailedLockException If the function was not able to acquire the lock due to a ZK error.
	 * @throws Exception ZooKeeper exception.
	 */
	protected synchronized boolean notifyFinishedMigration(String tableName, int VDPid) throws FailedLockException, Exception {
		try{
			if(!useZkServer2)
				while(!getZkServer().acquireLock(tableName)){
				//log.error(Thread.currentThread().getName()+
				//			" Cannot acquire lock on table: "+tableName+". Retrying!");
				}
			else
				while(!getZkServer2().acquireLock(tableName, VDPid+"")){}
			//log.info(Thread.currentThread().getName() + 
			//			" Got lock!");
		} catch(Exception e){
			throw new FailedLockException();
		}
		
		try{
			MigrationStatus migStatus = null;
			VDPmigrationStatus vdpMigStatus = null;
			VDPstatus migratedVDP;
			if(!useZkServer2){
				migStatus = getZkServer().getFreshMigrationStatus(tableName, null);
				migratedVDP = migStatus.finish_migrateVDP(VDPid);
			}else{
				vdpMigStatus = getZkServer2().getFreshMigrationStatus(tableName, VDPid+"", null);
				migratedVDP = vdpMigStatus.finish_migrateVDP();	
			}
			int retries = 0;
			if(!useZkServer2){
				boolean setted = false;
				while(!setted && retries < 10){
					setted = getZkServer().setFreshMigrationStatus(tableName, migStatus);
					if(!setted){
						log.error(Thread.currentThread().getName() + 
								" ==> !!! Couldn't update the migration status to "+migratedVDP.name()+" for VDP: "+tableName+"/"+VDPid);
						Thread.sleep(100);
						retries++;
					}
				}
				getZkServer().releaseLock(tableName);
			}else{
				boolean setted = false;
				while(!setted && retries<10){
					setted = getZkServer2().setFreshMigrationStatus(tableName, VDPid+"", vdpMigStatus);
					if(!setted){
						log.error(Thread.currentThread().getName() + 
								" ==> !!! Couldn't update the migration status to "+migratedVDP.name()+" for VDP: "+tableName+"/"+VDPid);
						Thread.sleep(100);
						retries++;
					}
				}
				getZkServer2().releaseLock(tableName, VDPid+"");
			}
			log.debug(Thread.currentThread().getName() +
					" updated migration status to "+migratedVDP.name()+" for VDP: "+tableName+"/"+VDPid);
			if(!useZkServer2)
				return migStatus.getVDPstatus(VDPid).getCurrentState().name().equals(VDPstatus.MIGRATED.name()) && retries < 10 ? true : false;
			else
				return vdpMigStatus.getVDPstatus().getCurrentState().name().equals(VDPstatus.MIGRATED.name()) && retries < 10 ? true : false;
		} catch (Exception e) {
			if(!useZkServer2)
				getZkServer().releaseLock(tableName);
			else
				getZkServer2().releaseLock(tableName, VDPid+"");
			throw new Exception(e);
		}
	}
	
	/**
	 * Returns a list containing all the tables of the database.
	 * @return The list of tables.
	 */
	public abstract List<String> getTableList();
	
	/**
	 * When migrated in a partitioned way, this method MUST be called by the TWC in order to 
	 * report that an entity has been migrated.
	 * The method updates the counters relative to the VDP that contains the entity.
	 * @param myModel The Metamodel entity.
	 */
	public void updateVDPsCounters(Metamodel myModel){
		if(myModel==null) return;
		Integer VDPid = VdpUtils.getVDP(Integer.parseInt(myModel.getRowKey()), vdpSize);
		Map<String, Integer> vdpSizeMap = myModel.getActualVdpSize();
		Set<String> columnFamilies = vdpSizeMap.keySet();
		int size = (int) Math.pow(10, vdpSize);
		for(String cf : columnFamilies){
			int updatedValue = vdpsCounters.putAndIncrementCounter(cf, VDPid, size);
			//log.debug(Thread.currentThread().getName()+
			//		"\n updating VDPs Counters: "
			//		+"piggybacked value: "+vdpSizeMap.get(cf)
			//		+ " set counter "+cf+"/"+VDPid+" to: "+updatedValue);
			
			//check if we finished to migrate an entire VDP
			if(updatedValue>=vdpSizeMap.get(cf)){
				boolean proof = false;
				int retries = 0;
				while(!proof && retries <= 3){
					try {
						proof = notifyFinishedMigration(cf, VDPid);
					} catch (FailedLockException e) {
						log.error("{} - ZK error when trying to acquire lock!",
								Thread.currentThread().getName());
					} catch (Exception e) {
						log.error("Unable to update MigrationStatus for VDP "+VDPid+
								" in table "+cf, e);
						retries++;
					}
				}
			}
		}
	}
	
	public long getVDPsCountersTotal(){
		if(vdpsCounters!=null)
			return vdpsCounters.getTotalMigrated();
		
		return 0;
	}

	/**
	 * @return the pauseSource
	 */
	public PauseSource getPauseSource() {
		return pauseSource;
	}

	/**
	 * @return the slowdownSource
	 */
	public SlowDownSource getSlowdownSource() {
		return slowdownSource;
	}
	
	public void stopThreads(){
		if(!stopThreads)
			this.stopThreads = true;
	}
	
	public boolean isStopped(){
		return stopThreads ? true : false;
	}
	
	private boolean isZkServerConnected(){
		if(!useZkServer2)
			return zkServer==null ? false : true;
		else
			return zkServer2==null ? false : true;
	}
	
	private boolean isZkClientConnected(){
		return zkClient==null ? false : true;
	}
	
	private void connectZkServer(){
		if(isZkServerConnected() || connectString == null)
			return;
		if(!useZkServer2){
			synchronized(zkserver_lock){
				zkServer = new ZKserver(connectString);
			}
		}else{
			synchronized(zkserver2_lock){
				zkServer2 = new ZKserver2(connectString);
			}
		}
	}
	
	private void disconnectZkServer(){
		if(!isZkServerConnected())
			return;
		if(!useZkServer2){
			synchronized(zkserver_lock){
				zkServer.close();
				zkServer = null;
			}
		}else{
			synchronized(zkserver2_lock){
				zkServer2.close();
				zkServer2 = null;
			}
		}
	}
	
	private void connectZkClient(){
		if(isZkClientConnected() || connectString == null)
			return;
		synchronized(zkclient_lock){
			zkClient = new ZKclient(connectString);
		}
	}
	
	private void disconnectZkClient(){
		if(!isZkClientConnected())
			return;
		synchronized(zkclient_lock){
			zkClient.close();
			zkClient = null;
		}
	}
	
	private synchronized ZKserver getZkServer(){
		if(!isZkServerConnected())
			connectZkServer();
		return zkServer;
	}
	
	protected synchronized ZKserver2 getZkServer2(){
		if(!isZkServerConnected())
			connectZkServer();
		return zkServer2;
	}
	
	private synchronized ZKclient getZkClient(){
		if(!isZkClientConnected())
			connectZkClient();
		return zkClient;
	}
	
	private MigrationComponentStatus retrieveComponentStatus(String componentName){
		MigrationComponentStatus prev = zkStatuses.getMigrationComponentStatus(componentName, getMigrationId());
		if(prev==null){
			prev = new MigrationComponentStatus(getMigrationId(), componentName);
		}
		return prev;
	}
	
	//TODO: implement migration id support!
	private int getMigrationId(){
		return 1;
	}
	
	@Override
	public void onVdpMigrated(MigratedVdpEvent event) {
		if(event!=null){
			migratedTablesList.addIfAbsent(event.getTableName());
			log.debug("{} - Received VDP migrated Event on table {} ",
				Thread.currentThread().getName(),
				event.getTableName());
		}else{
			log.warn("{} - Received empty VDP migrated event",
					Thread.currentThread().getName());
		}
		//check if all tables have been migrated
		if(vdpsTablesCopy!=null && !vdpsTablesCopy.isEmpty()){
			if(migratedTablesList.containsAll(vdpsTablesCopy.keySet())){
				isMigrationFinished=true;
				log.info(Thread.currentThread().getName()+
						" - All tables have been migrated!");
				storedFinishedMigration("SRC");
				storedFinishedMigration("TWC");
				
			}
		}
	}
	
	public boolean isMigrationFinished(){
		return isMigrationFinished;
	}
	
	/**
	 * Retrieve the total VDP numbers 
	 * @param zk Initialized ZooKeeper instance
	 */
	private boolean populateVdpsTables(ZooKeeper zk){
		List<String> tables = zk.getTablesList();
		boolean flag = vdpsTables!=null && tables!=null && tables.size()>0;
		if(tables!=null){
			for(String table : tables){
				int value = zk.getTableVDPsNo(table)-1;
				if(value<0)
					flag=false;
				if(vdpsTables!=null){
					vdpsTables.put(table, value);
				}
			}
			vdpsTablesCopy = new ConcurrentHashMap<>(vdpsTables);	
		}
		return flag;
	}
}
