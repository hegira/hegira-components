/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.adapters.hbase;

import it.polimi.hegira.adapters.AbstractDatabase;
import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.exceptions.QueueException;
import it.polimi.hegira.models.HBaseModel;
import it.polimi.hegira.models.HBaseModel.HBaseCell;
import it.polimi.hegira.models.Metamodel;
import it.polimi.hegira.queue.TaskQueue;
import it.polimi.hegira.transformers.HBaseTransformer;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;
import it.polimi.hegira.utils.PropertiesManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.FirstKeyOnlyFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;

public class HBase extends AbstractDatabase {

	private static final Logger log = LoggerFactory.getLogger(HBase.class);
	int _batchsize=100;
	int _maxEntitiesInMemory=300;
	long consumeIntervalMillis=5000;

	private class ConnectionObject {
		public ConnectionObject() {
		}

		@SuppressWarnings("unused")
		public ConnectionObject(Connection connection) {
			this.connection = connection;
		}

		private Connection connection;
	}

	private int getThreadId() {
		int thread_id = 0;
		if (TWTs_NO != 0)
			thread_id = (int) (Thread.currentThread().getId()%TWTs_NO);
		return thread_id;
	}

	public Connection getConnection() {
		ConnectionObject cobj = connectionList.get(getThreadId());
		return cobj.connection;
	}

	private List<ConnectionObject> connectionList;

	private Configuration config;
	
	private org.apache.hadoop.hbase.client.Consistency consistencyType;

	public HBase(Map<String, String> options) {
		super(options);

		int size = TWTs_NO;
		if (size <= 0)
			size = 1;

		connectionList = Collections.synchronizedList(new  ArrayList<ConnectionObject>(size));
		for (int i = 0; i < size; ++i)
			connectionList.add(new ConnectionObject());

		initConfiguration();
		
		consistencyType = PropertiesManager.getHBaseConsistencyType();
		
		if(options.get("mode")!=null && options.get("mode").equals(Constants.CONSUMER)){
			log.debug("Changing RabbitMQ taskqueue basicQos to support prefetching");
			String memProp = PropertiesManager.getHBaseProperty("hegira.maxEntitiesInMem");
			if(memProp!=null){
				try{
				_maxEntitiesInMemory = Integer.parseInt(memProp);
				} catch (NumberFormatException e){}
			}
			
			_batchsize=_maxEntitiesInMemory/size;
			int i=0;
			for(TaskQueue queue: taskQueues){
				try {
					queue.changeBaseQosPerConsumer(_batchsize+1);
				} catch (IOException e) {
					log.error("Unable to change BaseQos for queue: {}", i);
				}
				i++;
			}
			
			String cons = PropertiesManager.getHBaseProperty("rabbitmq.consumeIntervallMillis");
			if(cons!=null){
				try{
					consumeIntervalMillis = Long.parseLong(cons);
				} catch (NumberFormatException e){}
			}
		}else{
			log.debug("Got mode: {}", options.get("mode"));
		}
	}

	private void initConfiguration() {
		config = HBaseConfiguration.create();
		/*String tmp = PropertiesManager.getZooKeeperConnectString();
		int i;
		if (tmp != null && tmp.length() > 0 && (i = tmp.indexOf(':')) > -1)
			config.set("hbase.zookeeper.quorum", tmp.substring(0, i));*/
		
		String hBaseQuorum = PropertiesManager.getHBaseQuorum();
		if(hBaseQuorum != null && hBaseQuorum.length() > 0){
			config.set("hbase.zookeeper.quorum", hBaseQuorum);
		}
		
		String zkCs = PropertiesManager.getZooKeeperConnectString();
		int i;
		if(zkCs != null && zkCs.length() > 0 && (i = zkCs.indexOf(":")) > -1){
			int end;
			int port;
			if((end = zkCs.indexOf(','))>-1){
				port = Integer.parseInt(zkCs.substring(i+1,end));
			}else{
				port = Integer.parseInt(zkCs.substring(i+1));
			}
			config.setInt("hbase.zookeeper.property.clientPort", port);
		}else{
			config.setInt("hbase.zookeeper.property.clientPort", 2181);
		}
	}

	@Override
	protected AbstractDatabase fromMyModel(Metamodel mm) {
		//TWC
		log.debug("{} Hi I'm the HBase consumer!", Thread.currentThread().getName());
		
		//Instantiate the Thrift Deserializer
		TDeserializer deserializer = new TDeserializer(new TBinaryProtocol.Factory());
		int thread_id = getThreadId();
		
		int count = 0;
		
		while (true) {
			try {

				log.debug("{} - getting taskQueue with id: {}", Thread.currentThread().getName(), thread_id);
				Delivery delivery = taskQueues.get(thread_id).getConsumer().nextDelivery();
				if (delivery!=null) {
					Metamodel myModel = new Metamodel();
					deserializer.deserialize(myModel, delivery.getBody());
					
					HBaseTransformer transformer = new HBaseTransformer();
					HBaseModel models = transformer.fromMyModel(myModel);
					
					List<HBaseCell> cells = models.getCells();
					
					log.debug("{} Inserting {} entities", Thread.currentThread().getName(), cells.size());
					
					if (cells.size() == 0) {
						taskQueues.get(thread_id).sendNack(delivery);
						continue;
					}
					
					for (HBaseCell c : cells) {
						addCell(c);
						count++;
					}
					
					if(count%2000==0)
						log.debug("{} Inserted {} entities", Thread.currentThread().getName(), count);
					
					taskQueues.get(thread_id).sendAck(delivery);
					
				} else {
					log.debug("{} - The queue {} is empty", Thread.currentThread().getName(),
							TaskQueue.getDefaultTaskQueueName());
				}
			} catch (ShutdownSignalException | ConsumerCancelledException
					| InterruptedException e) {
				log.error(Thread.currentThread().getName() + " - Cannot read next delivery from the queue " + 
					TaskQueue.getDefaultTaskQueueName(), e);
			} catch (TException e) {
				log.error(Thread.currentThread().getName() + " - Error deserializing message ", e);
			} catch (IOException e) {
				log.error(Thread.currentThread().getName() + " - Error storing data on hbase ", e);
			} catch (QueueException e) {
				log.error(Thread.currentThread().getName() + " - Error sending an acknowledgment to the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
			}
		}
	}

	@Override
	protected AbstractDatabase fromMyModelPartitioned(Metamodel mm) {
		//Instantiate the Thrift Deserializer
		TDeserializer deserializer = new TDeserializer(new TBinaryProtocol.Factory());
		int thread_id = Integer.parseInt(Thread.currentThread().getName());	
		
		while(true && !stopThreads){
			int randomNum = ThreadLocalRandom.current().nextInt(5000, 10000 + 1);;
			List<Long> deliveryTags = new ArrayList<Long>(_batchsize);
			//K=tableKey, V=cells list
			Map<String, List<HBaseCell>> hbaseCells = new HashMap<String, List<HBaseCell>>();
			List<Metamodel> mmList = new ArrayList<Metamodel>(_batchsize);
			int batchcounter=0;
			Delivery delivery = null;
				//for(Delivery delivery = taskQueues.get(thread_id).consume(3000); 
				//		batchcounter <_batchsize && delivery != null; 
				//		batchcounter++, delivery = taskQueues.get(thread_id).consume(3000)){
			do{
				try {
					delivery = taskQueues.get(thread_id).consume(consumeIntervalMillis+randomNum);
					if(delivery!=null && !stopThreads){
						try{
							Metamodel myModel = new Metamodel();
							deserializer.deserialize(myModel, delivery.getBody());
							
							HBaseTransformer transformer = new HBaseTransformer();
							HBaseModel models = transformer.fromMyModel(myModel);
							
							List<HBaseCell> cells = models.getCells();
							
												
							if (cells.size() == 0) {
								taskQueues.get(thread_id).sendNack(delivery);
								String key = null, table = null;
								if(myModel!=null){
									key = myModel.rowKey;
									if(myModel.columnFamilies!=null && !myModel.columnFamilies.isEmpty())
										table = myModel.columnFamilies.get(0);
								}
								
								log.error("{} - Error while converting metamodel entities ({}/{}) towards HBase entities."
										+ " VDPs status may not be properly updated!",
										Thread.currentThread().getName(),
										table,key);
								continue;
							}
							
							deliveryTags.add(new Long(delivery.getEnvelope().getDeliveryTag()));
							mmList.add(myModel);
							
							for(HBaseCell cell : cells){
								List<HBaseCell> list = hbaseCells.get(cell.getTableKey());
								if(list==null){
									list=new ArrayList<HBaseCell>();
								}
								list.add(cell);
								hbaseCells.put(cell.getTableKey(), list);
							}
						} catch (TException e) {
							log.error(Thread.currentThread().getName() + " - Error deserializing message ", e);
						} catch (QueueException e) {
							log.error(Thread.currentThread().getName() + " - Error sending an acknowledgment to the queue " + 
									TaskQueue.getDefaultTaskQueueName(), e);
						}
					}else{
						log.debug(Thread.currentThread().getName() + " - The queue " +
								TaskQueue.getDefaultTaskQueueName() + " is empty");
					}
				} catch (ShutdownSignalException | ConsumerCancelledException
						| InterruptedException e) {
					log.error(Thread.currentThread().getName() + " - Cannot read next delivery from the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
				}
				
				batchcounter++;
			} while(batchcounter <_batchsize && delivery != null);
			
			
			for(String tblKey : hbaseCells.keySet()){
				//log.debug("{} - Storing HBase Cells with tableKey: {}",
				//		Thread.currentThread().getName(), tblKey);
				try {
					storeCells(hbaseCells.get(tblKey), tblKey);
					//log.debug("{} - StorED HBase Cells with tableKey: {}",
					//		Thread.currentThread().getName(), tblKey);
				} catch (IOException e) {
					log.error(Thread.currentThread().getName() + " - Error storing data in HBase ", e);
				}
			}
			
			try {
				//log.debug("{} - Acknowledging {} messages",
				//		Thread.currentThread().getName(),
				//		deliveryTags.size());
				taskQueues.get(thread_id).sendAcks(deliveryTags);
				//log.debug("{} - AcknowledgED {} messages",
				//		Thread.currentThread().getName(),
				//		deliveryTags.size());
			} catch (QueueException e) {
				log.error(Thread.currentThread().getName() + " - Error sending cumulative acknowledgments to the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
			}
			
			//log.debug("{} - Updating VDP counters for {} entities",
			//		Thread.currentThread().getName(),
			//		mmList.size());
			for(Metamodel ent : mmList){
				updateVDPsCounters(ent);
			}
			//log.debug("{} - UpdatED VDP counters for {} entities",
			//		Thread.currentThread().getName(),
			//		mmList.size());
			
			if(isMigrationFinished())
				stopThreads();
			
			//-----------------------
			/*try{
				Delivery delivery = taskQueues.get(thread_id).consume();
				if(delivery!=null && !stopThreads){
					Metamodel myModel = new Metamodel();
					deserializer.deserialize(myModel, delivery.getBody());
					
					HBaseTransformer transformer = new HBaseTransformer();
					HBaseModel models = transformer.fromMyModel(myModel);
					
					List<HBaseCell> cells = models.getCells();
										
					if (cells.size() == 0) {
						taskQueues.get(thread_id).sendNack(delivery);
						String key = null, table = null;
						if(myModel!=null){
							key = myModel.rowKey;
							if(myModel.columnFamilies!=null && !myModel.columnFamilies.isEmpty())
								table = myModel.columnFamilies.get(0);
						}
						
						log.error("{} - Error while converting metamodel entities ({}/{}) towards HBase entities."
								+ " VDPs status may not be properly updated!",
								Thread.currentThread().getName(),
								table,key);
						continue;
					}
					
					for (HBaseCell c : cells) {
						storeCell(c);
					}
					
					taskQueues.get(thread_id).sendAck(delivery);
					//incrementing the VDPsCounters
					updateVDPsCounters(myModel);
				} else {
					log.debug(Thread.currentThread().getName() + " - The queue " +
							TaskQueue.getDefaultTaskQueueName() + " is empty");
				}
			} catch (ShutdownSignalException | ConsumerCancelledException
					| InterruptedException e) {
				log.error(Thread.currentThread().getName() + " - Cannot read next delivery from the queue " + 
					TaskQueue.getDefaultTaskQueueName(), e);
			} catch (TException e) {
				log.error(Thread.currentThread().getName() + " - Error deserializing message ", e);
			} catch (QueueException e) {
				log.error(Thread.currentThread().getName() + " - Error sending an acknowledgment to the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
			} catch (IOException e) {
				log.error(Thread.currentThread().getName() + " - Error storing data in HBase ", e);
			}*/
		}
		
		
		return null;
	}

	@Override
	protected Metamodel toMyModel(AbstractDatabase db) {
		HBase hbase = (HBase) db;
		
		int thread_id = getThreadId();
		TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
		HBaseTransformer transformer = new HBaseTransformer(consistencyType);
		
		ModelIterator models;
		try {
			models = hbase.getModelIterator();
		} catch (Exception e) {
			log.error(Thread.currentThread().getName() + " - Error reading data from hbase.", e);
			return null;
		}
		
		int count = 0;
		int partialCount = 0;
		
		while (models.hasNext()) {
			HBaseModel model = models.next();
			Metamodel meta = transformer.toMyModel(model);
			try {
				taskQueues.get(thread_id).publish(serializer.serialize(meta));
				partialCount += model.size();
				
//				if (partialCount >= 5000) {
//					count += partialCount;
//					partialCount = 0;
//					taskQueues.get(0).slowDownProduction();
//				}
				
				log.debug("{} Produced: {} entities", Thread.currentThread().getName(), count + partialCount);
			} catch (QueueException e) {
				log.error(Thread.currentThread().getName() + " - Error communicating with the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
			} catch (TException e) {
				log.error(Thread.currentThread().getName() + " - Error serializing message ", e);
			}
		}
		
		count += partialCount;
		
		log.debug("{} ==> Transferred {} entities", Thread.currentThread().getName(), count);
		
		return null;
	}

	/**
	 * Checks if a connection has already been established
	 * 
	 * @return true if connected, false if not.
	 */
	public boolean isConnected() {
		try {
			ConnectionObject cobj = connectionList.get(getThreadId());
			return (cobj != null && cobj.connection != null && !cobj.connection
					.isClosed());
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void connect() throws ConnectException {
		int thread_id = getThreadId();
		if (!isConnected()) {
			try {
				connectionList.get(thread_id).connection = ConnectionFactory
						.createConnection(config);

				log.debug("{} - Connected - Added connection object at position: {}", Thread
						.currentThread().getName(), thread_id);
			} catch (IOException e) {
				log.error("Error while establishing the connection to HBase.",
						e);
				throw new ConnectException(DefaultErrors.connectionError);
			}
		} else {
			throw new ConnectException(DefaultErrors.alreadyConnected);
		}
	}

	@Override
	public void disconnect() {
		int thread_id = getThreadId();
		if (isConnected()) {
			if (connectionList.get(thread_id).connection != null) {
				try {
					connectionList.get(thread_id).connection.close();
				} catch (IOException e) {
					log.error("Error while disconnecting the connection.", e);
				}
			}
			connectionList.get(thread_id).connection = null;
			log.debug("{} Disconnected", Thread.currentThread().getName());
		} else {
			log.warn(DefaultErrors.notConnected);
		}
	}

	@Override
	protected Metamodel toMyModelPartitioned(AbstractDatabase db) {
		HBase hbase = (HBase) db;
		
		int thread_id = getThreadId();
		TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
		HBaseTransformer transformer = new HBaseTransformer();
		
		ModelIterator models;
		try {
			models = hbase.getModelIterator();
		} catch (Exception e) {
			log.error(Thread.currentThread().getName() + " - Error reading data from hbase.", e);
			return null;
		}
		
		int count = 0;
		int partialCount = 0;
		
		while (models.hasNext()) {
			HBaseModel model = models.next();
			Metamodel meta = transformer.toMyModel(model);
			try {
				taskQueues.get(thread_id).publish(serializer.serialize(meta));
				partialCount += model.size();
				
				if (partialCount >= 5000) {
					count += partialCount;
					partialCount = 0;
					taskQueues.get(0).slowDownProduction();
				}
				
				log.debug("{} Produced: {} entities", Thread.currentThread().getName(), count + partialCount);
			} catch (QueueException e) {
				log.error(Thread.currentThread().getName() + " - Error communicating with the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
			} catch (TException e) {
				log.error(Thread.currentThread().getName() + " - Error serializing message ", e);
			}
		}
		
		count += partialCount;
		
		log.debug("{} ==> Transferred {} entities", Thread.currentThread().getName(), count);
		
		return null;
	}

	@Override
	public List<String> getTableList() {
		if (isConnected()) {
			try {
				List<String> res = new ArrayList<String>();
				
				List<TableName> names = listTableNames();
				for (TableName name : names) {
					res.add(name.getNameAsString());
				}
				
				return res;
			} catch (Exception e) {
				log.error("Error while getting the table list.", e);
				return null;
			}
		} else {
			log.info(DefaultErrors.notConnected);
			return null;
		}
	}

	private List<TableName> listTableNames() throws IOException {
		List<TableName> res = new ArrayList<TableName>();

		Connection connection = getConnection();

		try (Admin admin = connection.getAdmin()) {
			TableName[] names = admin.listTableNames();
			for (TableName name : names)
				res.add(name);
		}

		return res;
	}

	private List<String> listRowNames(String tableKey) throws IOException {
		List<String> res = new ArrayList<String>();

		Connection connection = getConnection();

		try (Table table = connection.getTable(TableName.valueOf(tableKey))) {
			Scan scan = new Scan();
			scan.setConsistency(consistencyType);
			scan.setFilter(new FirstKeyOnlyFilter());
			scan.setCaching(1000);
			ResultScanner scanner = table.getScanner(scan);
			for (Result r : scanner)
				res.add(new String(r.getRow()));
		}

		return res;
	}
	
	private List<String> listColumnNames(String tableKey) throws IOException {
		List<String> res = new ArrayList<String>();
		
		List<String> families = listColumnFamiliesNames(tableKey);
		for (String family : families) {
			List<String> qualifiers = listColumnQualifiersNames(tableKey, family);
			for (String qualifier : qualifiers) {
				String name = String.format("%s:%s", family, qualifier);
				if (!res.contains(name))
					res.add(name);
			}
		}
		
		return res;
	}

	private List<String> listColumnFamiliesNames(String tableKey)
			throws IOException {
		List<String> res = new ArrayList<String>();

		Connection connection = getConnection();

		try (Table table = connection.getTable(TableName.valueOf(tableKey))) {
			HTableDescriptor desc = table.getTableDescriptor();
			HColumnDescriptor[] families = desc.getColumnFamilies();
			for (HColumnDescriptor family : families)
				res.add(family.getNameAsString());
		} catch (Exception e) { }

		return res;
	}

	private List<String> listColumnQualifiersNames(String tableKey, String rowKey,
			String columnFamily) throws IOException {
		List<String> res = new ArrayList<String>();

		Get get = new Get(Bytes.toBytes(rowKey));
		get.setConsistency(consistencyType);
		get.setMaxVersions();

		Connection connection = getConnection();

		try (Table table = connection.getTable(TableName.valueOf(tableKey))) {
			Result r = table.get(get);
			for (byte[] qualifier : r.getFamilyMap(Bytes.toBytes(columnFamily)).keySet()) {
				res.add(new String(qualifier));
			}
		}

		return res;
	}
	
	private List<String> listColumnQualifiersNames(String tableKey,
			String columnFamily) throws IOException {
		List<String> res = new ArrayList<String>();
		
		List<String> rows = listRowNames(tableKey);
		for (String rowKey : rows) {
			List<String> qualifiers = listColumnQualifiersNames(tableKey, rowKey, columnFamily);
			for (String qualifier : qualifiers) {
				if (!res.contains(qualifier))
					res.add(qualifier);
			}
		}
		
		return res;
	}

	private List<HBaseCell> getCells(String tableKey, String rowKey,
			String columnKey) throws IOException {
		List<HBaseCell> res = new ArrayList<HBaseCell>();

		Get get = new Get(Bytes.toBytes(rowKey));
		get.setConsistency(consistencyType);
		get.setMaxVersions();

		Connection connection = getConnection();

		try (Table table = connection.getTable(TableName.valueOf(tableKey))) {
			Result r = table.get(get);
			List<Cell> cells;
			if (columnKey != null) {
				String[] parts = columnKey.split(":");
				cells = r.getColumnCells(Bytes.toBytes(parts[0]), Bytes.toBytes(parts[1]));
			} else {
				cells = r.listCells();
			}

			for (Cell cell : cells) {
				GregorianCalendar d = new GregorianCalendar();
				d.setTimeInMillis(cell.getTimestamp());
				byte[] totalValueArray = cell.getValueArray();
				byte[] actualValue = new byte[cell.getValueLength()];
				int offset = cell.getValueOffset();
				for (int i = 0; i < actualValue.length; ++i)
					actualValue[i] = totalValueArray[i + offset];
				res.add(new HBaseCell(tableKey, rowKey, columnKey, actualValue, d));
			}
		}

		return res;
	}
	
	public ModelIterator getModelIterator() throws Exception {
		return new ModelIterator();
	}
	
	private class ModelIterator implements Iterator<HBaseModel> {
		
		private List<TableName> tables;
		private List<String> rows;
		private List<String> columns;
		private Iterator<HBaseModel> modelsPerRow;
		
		private String tableKey;
		private String rowKey;
		private String columnKey;
		
		public ModelIterator() throws Exception {
			tables = listTableNames();
			
			rows = new ArrayList<String>();
			columns = new ArrayList<String>();
		}
		
		private void loadNextChunkOfModelsIfNeeded() {
			while (modelsPerRow == null || !modelsPerRow.hasNext()) {
				while (columns.size() == 0 && rows.size() == 0 && tables.size() > 0) {
					tableKey = tables.remove(0).getNameAsString();
					try {
						rows = listRowNames(tableKey);
					} catch (Exception e) {
						throw new RuntimeException("Error while getting the rows names.", e);
					}
				}
				
				while (columns.size() == 0 && rows.size() > 0) {
					rowKey = rows.remove(0);
					try {
						columns = listColumnNames(tableKey);
					} catch (Exception e) {
						throw new RuntimeException("Error while getting the columns names.", e);
					}
				}
				
				if (columns.size() == 0 && rows.size() == 0 && tables.size() == 0)
					throw new RuntimeException("No more tables and rows to consider!");
				
				columnKey = columns.remove(0);
				
				try {
					modelsPerRow = HBaseModel.getModelsByCells(getCells(tableKey, rowKey, columnKey)).values().iterator();
				} catch (Exception e) {
					throw new RuntimeException("Error while trying to get the next chunk of models.", e);
				}
			}
		}

		@Override
		public boolean hasNext() {
			try { 
				loadNextChunkOfModelsIfNeeded();
			} catch (Exception e) {
				return false;
			}
			return modelsPerRow.hasNext();
		}
		
		@Override
		public HBaseModel next() {
			try { 
				loadNextChunkOfModelsIfNeeded();
			} catch (Exception e) {
				return null;
			}
			return modelsPerRow.next();
		}

		@Override
		public void remove() {
			log.warn("This is a read-only iterator!");
		}
		
	}

	private void addCell(HBaseCell cell) throws IOException {
		addCell(cell.getTableKey(), cell.getRowKey(), cell.getColumnKey(),
				cell.getValue(), cell.getTimestamp());
	}

	private void addCell(String tableKey, String rowKey, String columnKey,
			byte[] value, Calendar timestamp) throws IOException {
		String[] parts = columnKey.split(":");
		
		// TODO: use a cache layer instead
		addColumnFamily(tableKey, columnKey);
		
		Put put = new Put(Bytes.toBytes(rowKey));

		Connection connection = getConnection();

		try (Table table = connection.getTable(TableName.valueOf(tableKey))) {
			if (timestamp != null)
				put.addColumn(Bytes.toBytes(parts[0]), Bytes.toBytes(parts[1]),
						timestamp.getTimeInMillis(), value);
			else
				put.addColumn(Bytes.toBytes(parts[0]), Bytes.toBytes(parts[1]),
						value);
			table.put(put);
		}
	}
	
	@SuppressWarnings("unused")
	private void storeCell(HBaseCell cell) throws IOException{
		String tableKey = cell.getTableKey();
		String rowKey = cell.getRowKey();
		String columnKey = cell.getColumnKey();
		byte[] value = cell.getValue();
		Calendar timestamp = cell.getTimestamp();
		String[] cKeyParts = columnKey.split(":");
		String columnFamily = cKeyParts[0];
		String columnQualifier = cKeyParts[1];
		
		SchemaCache schemaCache = SchemaCache.getInstance();
		if(!schemaCache.containsTable(tableKey)){
			synchronized(this){
				createTable(tableKey, columnFamily);
				schemaCache.putColumnFamily(tableKey, columnFamily);
			}
		} else if(!schemaCache.containsColumnFamily(tableKey, columnFamily)){
			synchronized(this){
				createColumnFamily(tableKey, columnFamily);
				schemaCache.putColumnFamily(tableKey, columnFamily);
			}
		}
		
		//at this point both the table and the cf are present
		Put put = new Put(Bytes.toBytes(rowKey));

		try (Table table = getConnection().getTable(TableName.valueOf(tableKey))) {
			if (timestamp != null)
				put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnQualifier),
						timestamp.getTimeInMillis(), value);
			else
				put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnQualifier),
						value);
			table.put(put);
			table.close();
		}
	}
	
	private void storeCells(List<HBaseCell> cellsList, String tblKey) throws IOException{
		List<Put> putsList = new ArrayList<Put>(cellsList.size());
		for(HBaseCell cell : cellsList) {
			String tableKey = cell.getTableKey();
			String rowKey = cell.getRowKey();
			String columnKey = cell.getColumnKey();
			byte[] value = cell.getValue();
			Calendar timestamp = cell.getTimestamp();
			String[] cKeyParts = columnKey.split(":");
			String columnFamily = cKeyParts[0];
			String columnQualifier = cKeyParts[1];
			
			SchemaCache schemaCache = SchemaCache.getInstance();
			if(!schemaCache.containsTable(tableKey)){
				synchronized(this){
					createTable(tableKey, columnFamily);
					schemaCache.putColumnFamily(tableKey, columnFamily);
				}
			} else if(!schemaCache.containsColumnFamily(tableKey, columnFamily)){
				synchronized(this){
					createColumnFamily(tableKey, columnFamily);
					schemaCache.putColumnFamily(tableKey, columnFamily);
				}
			}
			
			//at this point both the table and the cf are present
			Put put = new Put(Bytes.toBytes(rowKey));
			if (timestamp != null)
				put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnQualifier),
						timestamp.getTimeInMillis(), value);
			else
				put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnQualifier),
						value);
			
			putsList.add(put);
		}
		
		if(putsList!=null){
			try (Table table = getConnection().getTable(TableName.valueOf(tblKey))) {
				table.put(putsList);
				table.close();
			}
		}
	}

	private void createColumnFamily(String tableKey, String columnFamily) throws IOException {
		try (Admin admin = getConnection().getAdmin()) {
			TableName name = TableName.valueOf(tableKey);
			HColumnDescriptor desc = new HColumnDescriptor(columnFamily);
			secureDisableTable(name, admin);
			admin.addColumn(name, desc);
			secureEnableTable(name, admin);
			admin.close();
		} 
	}

	private void createTable(String tableKey, String columnFamily) throws IOException {
		try (Admin admin = getConnection().getAdmin()) {
			TableName name = TableName.valueOf(tableKey);
			
			HColumnDescriptor desc = new HColumnDescriptor(columnFamily);
			if (!admin.tableExists(name)) {
				HTableDescriptor descTable = new HTableDescriptor(name);
				descTable.addFamily(desc);
				try {
					admin.createTable(descTable);
				} catch (org.apache.hadoop.hbase.TableExistsException e) {
					secureDisableTable(name, admin);
					
					try{
						admin.addColumn(name, desc);
					}catch(org.apache.hadoop.hbase.InvalidFamilyOperationException e1){
						log.debug("Column Family {} already exists for table {}", 
								desc.getNameAsString(), name);
					}
					
					secureEnableTable(name, admin);
				}
			}
			admin.close();
		}
	}

	private void addColumnFamily(String tableKey, String columnKey)
			throws IOException {
		String[] parts = columnKey.split(":");
		
		List<String> columns = listColumnFamiliesNames(tableKey);
		if (columns.contains(parts[0]))
			return;
		
		Connection connection = getConnection();

		try (Admin admin = connection.getAdmin()) {
			TableName name = TableName.valueOf(tableKey);
			
			HColumnDescriptor desc = new HColumnDescriptor(parts[0]);
			if (!admin.tableExists(name)) {
				HTableDescriptor descTable = new HTableDescriptor(name);
				descTable.addFamily(desc);
				try {
					admin.createTable(descTable);
				} catch (org.apache.hadoop.hbase.TableExistsException e) {
					secureDisableTable(name, admin);
					
					try{
						admin.addColumn(name, desc);
					}catch(org.apache.hadoop.hbase.InvalidFamilyOperationException e1){
						log.debug("Column Family {} already exists for table {}", 
								desc.getNameAsString(), name);
					}
					
					secureEnableTable(name, admin);
				}
			} else {
				secureDisableTable(name, admin);
				admin.addColumn(name, desc);
				secureEnableTable(name, admin);
			}
		}
	}

	private void secureDisableTable(TableName name, Admin admin){
		try {
			do{
				try{
					admin.disableTable(name);
					Thread.sleep(1);
				}catch(Exception e1){
					log.debug("Unable to disable table {}. Retrying...", name);
				}
			}while(!admin.isTableDisabled(name));
		} catch (IOException e) {
			// in case also the check on table returns an error
			log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "Cannot check if table {} is disabled. \n",name, e);
		}
	}
	
	private void secureEnableTable(TableName name, Admin admin){
		try {
			do{
				try{
					admin.enableTable(name);
				}catch(Exception e1){
					log.debug("Unable to enable table {}. Retrying...", name);
				}
			}while(admin.isTableDisabled(name));
		} catch (IOException e) {
			// in case also the check on table returns an error
			log.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
					+ "Cannot check if table {} is enabled. \n",name, e);
		}
	}
}
