/**
 * 
 */
package it.polimi.hegira.adapters.hbase;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Marco Scavuzzo
 *
 */
public class SchemaCache {
	//Key=tableName, Value=CFs map
	private ConcurrentHashMap<String, HashMap<String, Void>> tablesMap;
	
	private static class SchemaCacheHolder {
		private static final SchemaCache instance = new SchemaCache();
	}
	
	private SchemaCache() {
		tablesMap = new ConcurrentHashMap<String, HashMap<String, Void>>();
	}
	
	public static SchemaCache getInstance(){
		return SchemaCacheHolder.instance;
	}
	
	public synchronized void putColumnFamily(String table, String cf){
		if(table==null || cf==null)
			throw new IllegalArgumentException("Parameters cannot be null!");
		
		HashMap<String, Void> cfsMap = tablesMap.get(table);
		if(cfsMap==null)
			cfsMap = new HashMap<String, Void>();
		
		cfsMap.put(cf, null);
		tablesMap.put(table, cfsMap);
	}
	
	public synchronized void putTable(String table){
		if(table==null)
			throw new IllegalArgumentException("Parameter cannot be null!");
		
		tablesMap.putIfAbsent(table, new HashMap<String, Void>());
	}
	
	public boolean containsTable(String table){
		if(table==null)
			return false;
		return tablesMap.containsKey(table);
	}
	
	public boolean containsColumnFamily(String table, String cf){
		if(table==null || cf==null)
			return false;
		HashMap<String, Void> cfsMap = tablesMap.get(table);
		if(cfsMap==null)
			return false;
		
		return cfsMap.containsKey(cf);
	}
}
