/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.adapters.cassandra;

import it.polimi.hegira.adapters.AbstractDatabase;
import it.polimi.hegira.exceptions.ConnectException;
import it.polimi.hegira.exceptions.FailedLockException;
import it.polimi.hegira.exceptions.QueueException;
import it.polimi.hegira.models.CassandraColumn;
import it.polimi.hegira.models.CassandraModel;
import it.polimi.hegira.models.Column;
import it.polimi.hegira.models.Metamodel;
import it.polimi.hegira.queue.TaskQueue;
import it.polimi.hegira.transformers.CassandraTransformer;
import it.polimi.hegira.utils.CassandraTypesUtils;
import it.polimi.hegira.utils.ConfigurationManagerCassandra;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;
import it.polimi.hegira.utils.PropertiesManager;
import it.polimi.hegira.vdp.VdpUtils;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;
import com.datastax.driver.core.exceptions.AuthenticationException;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.datastax.driver.core.exceptions.UnsupportedFeatureException;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;
/**
 * 
 * @author Andrea Celli
 * @author Marco Scavuzzo
 */
public class Cassandra extends AbstractDatabase {
	private static Logger log = LoggerFactory.getLogger(Cassandra.class);
	
	private List<ConnectionObject> connectionList;
	
	private class ConnectionObject{
		protected Session session;
		public ConnectionObject(){}
		public ConnectionObject(Session session){
			this.session=session;
		}
	}
	
	/**
	 * The constructor creates a ConnectionObject for each
	 * thread and adds it to the connectionList
	 * @param options
	 */
	public Cassandra(Map<String, String> options){
		super(options);
		if(TWTs_NO>0){
			connectionList = Collections.synchronizedList(new  ArrayList<ConnectionObject>(TWTs_NO));
			//the creation of empty objects is needed to execute the method isConnected()
			for(int i=0;i<TWTs_NO;i++)
				connectionList.add(new ConnectionObject());
		}else{
			connectionList = Collections.synchronizedList(new ArrayList<ConnectionObject>(SRTs_NO));
			connectionList.add(new ConnectionObject());
		}
	}
	
	@Override
	public void connect() throws ConnectException {
		int thread_id=0;
		if(TWTs_NO!=0)
			thread_id=(int) (Thread.currentThread().getId()%TWTs_NO);
		
		if(!isConnected()){
			
			try{
				log.debug(Thread.currentThread().getName()+" - Logging into server");
				//
				//retrieves the unique session from the session manager
				//
				Session session=SessionManager.getSessionManager().getSession();
				ConnectionObject conObj= new ConnectionObject(session);
				//I use set in order to keep things in order with the empty connectioObjects
				connectionList.set(thread_id, conObj);
				
				log.debug(Thread.currentThread().getName()+" - Added connection object at "+
					"position: "+connectionList.indexOf(conObj)+
					" ThreadId%THREAD_NO="+thread_id);
			}catch(NoHostAvailableException | AuthenticationException | IllegalStateException ex){
				log.error(DefaultErrors.connectionError+"\nStackTrace:\n"+ex.getStackTrace());
				throw new ConnectException(DefaultErrors.connectionError);
			}
		}else{
			log.warn(DefaultErrors.alreadyConnected);
			//throw new ConnectException(DefaultErrors.alreadyConnected);
		}	
	}
	
	/**
	 * Checks if a connection has already been established for the current 
	 * thread
	 * @return true if connected, false if not.
	 */
	public boolean isConnected(){
		int thread_id=0;
		if(TWTs_NO!=0)
			thread_id=(int) (Thread.currentThread().getId()%TWTs_NO);
		//else if(SRTs_NO!=0)
		//	thread_id = (int) (Thread.currentThread().getId()%SRTs_NO);
		try{
			return (connectionList.get(thread_id).session==null) ? false : true;
		}catch(IndexOutOfBoundsException e){
			return false;
		}
	}
	
	@Override
	public void disconnect() {
		if(true)
			return;
		int thread_id = 0;
		if(TWTs_NO!=0)
			thread_id = (int) (Thread.currentThread().getId()%TWTs_NO);
		if(isConnected()){
			connectionList.get(thread_id).session.close();
			connectionList.get(thread_id).session = null;
			log.debug(Thread.currentThread().getName() + " Disconnected");
		}else
			log.warn(DefaultErrors.notConnected);	
	}

	
	@Override
	protected Metamodel toMyModel(AbstractDatabase model) {
		int thread_id = 0;
		if(TWTs_NO!=0)
			thread_id = (int) (Thread.currentThread().getId()%TWTs_NO);
		
		//Create a new instance of the Thrift Serializer
        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
        //Create a new instance of the cassandra transformer used to translate entities into the
        //metamodel format
        //the read consistency is taken from the cassandra configuration file
        CassandraTransformer cassandraTransformer;
        try{
        		String consistency=ConfigurationManagerCassandra.getConfigurationProperties(Constants.READ_CONSISTENCY);
        		cassandraTransformer=new CassandraTransformer(consistency);
        }catch(IllegalArgumentException e){
        		log.error("Consistency type not supported, check the configuration file");
        		return null;
        }
		
		Session session=connectionList.get(thread_id).session;
		//
		//TODO: aggiungere try/catch sulla lista di tables
		//
		// get the list of all tables contained in the keyspace
		Cluster cluster=session.getCluster();
		String keySpace=ConfigurationManagerCassandra.getConfigurationProperties(Constants.KEYSPACE);
		Collection<TableMetadata> tables=cluster
				.getMetadata()
				.getKeyspace(keySpace)
				.getTables();
		//FIXME trying to make it more general
		String fixedPrimaryKeyName=ConfigurationManagerCassandra.getConfigurationProperties(Constants.PRIMARY_KEY_NAME);
		
		for(TableMetadata table:tables){
			//get the name of the table
			String tableName=table.getName();

			try{
				//QUERY all the rows in the actual table
				ResultSet queryResults=session.execute("SELECT * FROM "+tableName);
				//
				//transformation to the metamodel for each row
				//
				for(Row row : queryResults){
					//create a new cassandra model instance
					CassandraModel cassModel = new CassandraModel();
					//set the table
					cassModel.setTable(tableName);
					//set the key
					//the primary key name has to be set by default (see limitations)
					
					//FIXME - General solution
					List<ColumnMetadata> pks = table.getPrimaryKey();
					//values composing the PK should be converted into a single string!
					String pkStr = CassandraTypesUtils.primaryKeyToString(pks, row);
					cassModel.setKeyValue(pkStr);
					//--------------------------------------
					
					//OLD
					//String key=row.getString(primaryKeyName);
					//cassModel.setKeyValue(key);
					
					//
					//COLUMNS
					//
					Iterator<ColumnDefinitions.Definition> columnsIterator=row.getColumnDefinitions().iterator();
					while(columnsIterator.hasNext()){
						ColumnDefinitions.Definition column=columnsIterator.next();
						
						//don't add the fixed primary key (id) column
						if(!column.getName().equals(fixedPrimaryKeyName)){
							CassandraColumn cassColumn=new CassandraColumn();
							
							//set name
							String columnName=column.getName();
							cassColumn.setColumnName(columnName);
							try{
								//set type + value 
								String dataType=column.getType().toString();
								//log.debug("Column {} type {}", columnName, dataType);
								setValueAndType(cassColumn,columnName,dataType,row);
								//set indexed 
								//if(table.getColumn(columnName).getIndex()!=null){
								if(table.getIndex(columnName)!=null){
									cassColumn.setIndexed(true);
								}else{
									cassColumn.setIndexed(false);
								}
								//add to the cassandra model
								cassModel.addColumn(cassColumn);
								
							}catch(ClassNotFoundException ex){
								log.error(Thread.currentThread().getName() + " - Error managing column: "+column.getName()
										+" for row: "+pkStr
										+" on cassandra table: "+tableName, ex);
							}
						}
					}
					//from cassandraModel to MetaModel
					Metamodel meta=cassandraTransformer.toMyModel(cassModel);
					
					
					//TODO: CHANGE ME!!!!
					boolean debug = false;
					if(!debug){
						try{
							//serialize & add to the queue
							taskQueues.get(thread_id).publish(serializer.serialize(meta));
						}catch (QueueException e) {
							log.error(Thread.currentThread().getName() + " - Error communicating with the queue " + 
									TaskQueue.getDefaultTaskQueueName(), e);
						} catch (TException e) {
							log.error(Thread.currentThread().getName() + " - Error serializing message ", e);
						}
					}else{
						log.info("DEBUG MODE - Converted row {} into Metamodel format.", pkStr);
					}
			  }
			}catch(NoHostAvailableException | 
					QueryExecutionException |
					QueryValidationException | 
					UnsupportedFeatureException ex){
				log.error(Thread.currentThread().getName() + " - Error during the query on cassandra table: "+tableName, ex);
			}
			
		}
		
		return null;
	}
	

	/**
	 * This method set the type of the cassandra column and, according to the specific type, it retrieve and sets the value
	 * @param cassandraColumn
	 * @param columnName
	 * @param dataType -  CQL data type
	 * @param row
	 * @throws TypeNotPresentException
	 */
	private void setValueAndType(CassandraColumn cassandraColumn, String columnName,String dataType, Row row) throws ClassNotFoundException{
		
		if(!CassandraTypesUtils.isCollection(dataType)){
			//log.debug("Column {} is NOT a collection!", columnName);
			setValueAndSimpleType(cassandraColumn,  columnName, dataType, row);
		}else{
			//log.debug("Column {} IS a collection!", columnName);
			setValueAndCollectionType(cassandraColumn, columnName, dataType, row);
		}
		
	}

	/**
	 * 1) simple CQL type --> java type & set the type in cassandra model column
	 * 2) retrieve the value on the base of the java type
	 * @param cassandraColumn
	 * @param columnName
	 * @param dataType - string containing the CQL type
	 * @param row
	 * @throws ClassNotFoundException
	 */
	private void setValueAndSimpleType(CassandraColumn cassandraColumn,
			String columnName, String dataType, Row row) throws ClassNotFoundException{
		switch(dataType){
		case "ascii": 
			cassandraColumn.setValueType("String");
			cassandraColumn.setColumnValue(row.getString(columnName));
			return;
		case "bigint":
			cassandraColumn.setValueType("Long");
			cassandraColumn.setColumnValue(row.getLong(columnName));
			return;
		case "blob":
			cassandraColumn.setValueType("byte[]");
			//TODO: check
			cassandraColumn.setColumnValue(row.getBytes(columnName));
			return;
		case "boolean":
			cassandraColumn.setValueType("Boolean");
			cassandraColumn.setColumnValue(row.getBool(columnName));
			return;
		case "counter":
			cassandraColumn.setValueType("Long");
			cassandraColumn.setColumnValue(row.getLong(columnName));
			return;
		case "decimal":
			cassandraColumn.setValueType("BigDecimal");
			cassandraColumn.setColumnValue(row.getDecimal(columnName));
			return;
		case "double":
			cassandraColumn.setValueType("Double");
			cassandraColumn.setColumnValue(row.getDouble(columnName));
			return;
		case "float":
			cassandraColumn.setValueType("Float");
			cassandraColumn.setColumnValue(row.getFloat(columnName));
			return;
		case "inet":
			cassandraColumn.setValueType("InetAddress");
			cassandraColumn.setColumnValue(row.getInet(columnName));
			return;
		case "int":
			cassandraColumn.setValueType("Integer");
			cassandraColumn.setColumnValue(row.getInt(columnName));
			return;
		case "text":
			cassandraColumn.setValueType("String");
			cassandraColumn.setColumnValue(row.getString(columnName));
			return;
		case "timestamp":
			cassandraColumn.setValueType("Date");
			cassandraColumn.setColumnValue(row.getDate(columnName));
			return;
		case "uuid":
			cassandraColumn.setValueType("UUID");
			cassandraColumn.setColumnValue(row.getUUID(columnName));
			return;
		case "varchar":
			cassandraColumn.setValueType("String");
			cassandraColumn.setColumnValue(row.getString(columnName));
			return;
		case "varint":
			cassandraColumn.setValueType("BigInteger");
			cassandraColumn.setColumnValue(row.getVarint(columnName));
			return;
		case "timeuuid":
			cassandraColumn.setValueType("UUID");
			cassandraColumn.setColumnValue(row.getUUID(columnName));
			return;
		case "udt":
			cassandraColumn.setValueType("UDTValue");
			cassandraColumn.setColumnValue(row.getUDTValue(columnName));
			return;
		case "tuple":
			cassandraColumn.setValueType("TupleValue");
			cassandraColumn.setColumnValue(row.getTupleValue(columnName));
			return;
		case "custom":
			//TODO: check
			//TODO: check docs, I said custom types were not supported
			//log.debug("Setting CUSTOM type ByteBuffer for column {} that had type {}",
			//		columnName,
			//		dataType);
			cassandraColumn.setValueType("ByteBuffer");
			cassandraColumn.setColumnValue(row.getBytes(columnName));
			return;
		default: 
			throw  new ClassNotFoundException();
		}
		
	}
	
	/**
	 * Manage the conversion of a CQL collection type to java type
     * 1) simple CQL type --> java type & set the type in cassandra model column
	 * 2) retrieve the value on the base of the java type
	 * 
	 * @param cassandraColumn
	 * @param columnName
	 * @param dataType - collection type in the form X<Y, Z> or X<Y>
	 * @param row
	 * @throws ClassNotFoundException
	 */
	private void setValueAndCollectionType(CassandraColumn cassandraColumn,
			String columnName,String dataType, Row row) throws ClassNotFoundException{
		
		//check if the collection is one of the supported types
		CassandraTypesUtils.isSupportedCollection(dataType);
		
		String collectionType=CassandraTypesUtils.getCollectionType(dataType);
		
		if(collectionType.equals("map")){
			//log.debug("The collection for column {} of type {} is a MAP", columnName,
			//		dataType);
			String CQLSubType1=CassandraTypesUtils.getFirstSimpleType(dataType);
			//retrieve the second subtype removing spaces in the string
			String CQLSubType2=CassandraTypesUtils.getSecondSimpleType(dataType).replaceAll("\\s","");
			//Set the column type
			cassandraColumn.setValueType("Map<"+CassandraTypesUtils.getJavaSimpleType(CQLSubType1)+","+
					CassandraTypesUtils.getJavaSimpleType(CQLSubType2)+">");
			//set the column value
			cassandraColumn.setColumnValue(row.getMap(columnName, Object.class, Object.class));
		}else{
			//the collection has only one subtype
			//retrieve the subtype
			String CQLSubType=dataType.substring(dataType.indexOf("<")+1,dataType.indexOf(">"));
			if(collectionType.equals("set")){
				//log.debug("The collection for column {} of type {} is a SET", columnName,
				//		dataType);
				//set type
				cassandraColumn.setValueType("Set<"+CassandraTypesUtils.getJavaSimpleType(CQLSubType)+">");
				//set the value
				cassandraColumn.setColumnValue(row.getSet(columnName, Object.class));
			}else{
				if(collectionType.equals("list")){
					//log.debug("The collection for column {} of type {} is a LIST", columnName,
					//		dataType);
					//set type
					cassandraColumn.setValueType("List<"+CassandraTypesUtils.getJavaSimpleType(CQLSubType)+">");
					//set the value
					cassandraColumn.setColumnValue(row.getList(columnName, Object.class));
				}else{
					log.error("The collection for column {} of type {} is a neither MAP nor LIST nor SET (what is it then??)", 
							columnName,
							dataType);
				}
			}
		}
		
	}

	@Override
	protected AbstractDatabase fromMyModel(Metamodel mm) {
		log.debug(Thread.currentThread().getName()+" Cassandra consumer started ");
		
		//Thrift Deserializer
		TDeserializer deserializer = new TDeserializer(new TBinaryProtocol.Factory());
		//retrieve thread number
		int thread_id=0;
		if(TWTs_NO!=0){
			thread_id=(int) (Thread.currentThread().getId()%TWTs_NO);
		}
		//instantiate the cassandra transformer
		//the consistency level is not needed. Entity inserted with eventual consistency
		CassandraTransformer transformer=new CassandraTransformer();
		//instantiate the TableManager
		TablesManager tablesManager=TablesManager.getTablesManager();
		
		while(true){
			log.debug(Thread.currentThread().getName()+" Extracting from the taskQueue"+thread_id+
					" TWTs_NO: "+TWTs_NO);
			try{
				//extract from the task queue
				Delivery delivery=taskQueues.get(thread_id).getConsumer().nextDelivery();
				if(delivery!=null){
					//deserialize and retrieve the metamodel
					Metamodel metaModel=new Metamodel();
					deserializer.deserialize(metaModel, delivery.getBody());
					//retrieve the Cassandra Model
					CassandraModel cassandraModel=transformer.fromMyModel(metaModel);
				
					//retrieve the table and tries perform the insert
					try{
						tablesManager.getTable(cassandraModel.getTable()).insert(cassandraModel);
					}catch(ConnectException ex){
						log.error(Thread.currentThread().getName() + " - Not able to connect to Cassandra", ex);
						//nack
						taskQueues.get(thread_id).sendNack(delivery);
						log.info("Sending Nack!! for entity(/ies)");
					}catch(ClassNotFoundException ex){
						log.error(Thread.currentThread().getName() + " - Error in during the insertion -", ex);
						//nack
						taskQueues.get(thread_id).sendNack(delivery);
						log.info("Sending Nack!! for entity(/ies)");
					}
					//send ack
					taskQueues.get(thread_id).sendAck(delivery);
					
				}else{
					log.debug(Thread.currentThread().getName() + " - The queue " +
							TaskQueue.getDefaultTaskQueueName() + " is empty");
					return null;
				}
			}catch(ShutdownSignalException |
					ConsumerCancelledException |
					InterruptedException ex){
				log.error(Thread.currentThread().getName() + " - Cannot read next delivery from the queue " + 
						TaskQueue.getDefaultTaskQueueName(), ex);
			}catch(TException ex){
				log.error(Thread.currentThread().getName() + " - Error deserializing message ", ex);
			}catch(QueueException ex){
				log.error(Thread.currentThread().getName() + " - Error sending an acknowledgment to the queue " + 
						TaskQueue.getDefaultTaskQueueName(), ex);
			}
		}
	}

	@Override
	protected AbstractDatabase fromMyModelPartitioned(Metamodel mm) {
		TDeserializer deserializer = new TDeserializer(new TBinaryProtocol.Factory());
		//retrieve thread number
		int thread_id=0;
		if(TWTs_NO!=0){
			thread_id=(int) (Thread.currentThread().getId()%TWTs_NO);
		}
		//the consistency level is not needed. Entity inserted with eventual consistency
		CassandraTransformer transformer=new CassandraTransformer();
		//instantiate the TableManager
		TablesManager tablesManager=TablesManager.getTablesManager();
				
		while(true && !stopThreads){
			Delivery delivery;
			try {
				delivery = taskQueues.get(thread_id).consume();
				if(delivery!=null && !stopThreads){
					Metamodel myModel = new Metamodel();
					deserializer.deserialize(myModel, delivery.getBody());
					CassandraModel cassandraModel=transformer.fromMyModel(myModel);
					//retrieve the table and tries perform the insert
					try{
						tablesManager.getTable(cassandraModel.getTable()).insert(cassandraModel);
					}catch(ConnectException ex){
						log.error(Thread.currentThread().getName() + " - Not able to connect to Cassandra", ex);
						//nack
						taskQueues.get(thread_id).sendNack(delivery);
						log.info("Sending Nack!! for entity(/ies)");
					}catch(ClassNotFoundException ex){
						log.error(Thread.currentThread().getName() + " - Error in during the insertion -", ex);
						//nack
						taskQueues.get(thread_id).sendNack(delivery);
						log.info("Sending Nack!! for entity(/ies)");
					}
					//send ack
					taskQueues.get(thread_id).sendAck(delivery);
					//incrementing the VDPsCounters
					updateVDPsCounters(myModel);	
					////////////////////////////////
				}else{
					log.debug(Thread.currentThread().getName() + " - The queue " +
							TaskQueue.getDefaultTaskQueueName() + " is empty");
				}
			} catch (ShutdownSignalException | ConsumerCancelledException
					| InterruptedException e) {
				log.error(Thread.currentThread().getName() + " - Cannot read next delivery from the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
			} catch (TException e) {
				log.error(Thread.currentThread().getName() + " - Error deserializing message ", e);
			} catch (QueueException e) {
				log.error(Thread.currentThread().getName() + " - Error sending an acknowledgment to the queue " + 
						TaskQueue.getDefaultTaskQueueName(), e);
			}	
		}
		if(stopThreads)
			log.info("{} - Received Stop Thread Request!",Thread.currentThread().getName());
		return null;
	}

	@Override
	protected Metamodel toMyModelPartitioned(AbstractDatabase model) {
		Set<String> tables;
		if(!useZkServer2)
			tables = snapshot.keySet();
		else
			tables = new HashSet<String>(getZkServer2().getTableList());
		//int thread_id = (int) (Thread.currentThread().getId()%SRTs_NO);
		int thread_id =  0;
		Collection<String> filteredTables = filter(tables);
		
		//Create a new instance of the cassandra transformer used to translate entities into the
        //metamodel format
        //the read consistency is taken from the cassandra configuration file
        CassandraTransformer cassandraTransformer;
        try{
        		String consistency=ConfigurationManagerCassandra.getConfigurationProperties(Constants.READ_CONSISTENCY);
        		cassandraTransformer=new CassandraTransformer(consistency);
        }catch(IllegalArgumentException e){
        		log.error("Consistency type not supported, check the configuration file");
        		return null;
        }
		
		Session session=connectionList.get(thread_id).session;
		//FIXME trying to make it more general
		String fixedPrimaryKeyName=ConfigurationManagerCassandra.getConfigurationProperties(Constants.PRIMARY_KEY_NAME);
		Cluster cluster=session.getCluster();
		String keySpace=ConfigurationManagerCassandra.getConfigurationProperties(Constants.KEYSPACE);		
		
		for(String tableName : filteredTables){
			long i=0; //entites extracted per table
			//Create a new instance of the Thrift Serializer
	        TSerializer serializer = new TSerializer(new TBinaryProtocol.Factory());
	        int maxSeq,totalVDPs;
	        if(!useZkServer2){
	        	//retrieving the total number of entities written so far for this kind.
	        	maxSeq = snapshot.get(tableName).getLastSeqNr();
	        	//calculating the total number of VDPs for this kind
	        	totalVDPs = snapshot.get(tableName).getTotalVDPs(vdpSize);
	        }else{
	        	//trick!
	        	maxSeq = snapshot2.get(tableName+"1").getLastSeqNr();
	        	totalVDPs = snapshot2.get(tableName+"1").getTotalVDPs(vdpSize);
	        }
	        
	        Integer VDPid;
			try {
				VDPid = vdpsQueue.poll(tableName, 1, TimeUnit.MINUTES);
			} catch (InterruptedException e1) {
				VDPid = null;
				log.error("{} - VDPs extraction was interrupted. Stack Trace:\n{}", Thread.currentThread().getName(),e1);
			}
	        //extracting entities per each VDP
			
			do{
				try{
					log.debug("{} - Checking VDP {}/{}",
    						Thread.currentThread().getName(),
    						tableName, VDPid);	
					if(VDPid!=null && !stopThreads){
						if(canMigrate(tableName, VDPid) && !stopThreads){
							//generating ids from the VDP
							ArrayList<Integer> vdpElements = VdpUtils.getElements(VDPid, maxSeq, vdpSize);
							if(VDPid == 0){
								if(vdpElements.get(0) == 0)
									vdpElements.remove(0);
									//vdpElements.set(0, 1);
							}
							log.debug(Thread.currentThread().getName() +
									" Getting entities for VDP: "+tableName+"/"+VDPid);
							

							//QUERY all the rows in the actual table
							log.debug("Query: "+"SELECT * FROM "+tableName+
									" WHERE "+fixedPrimaryKeyName+" IN ("+listToString(vdpElements)+")");
							ResultSet queryResults=session.execute("SELECT * FROM "+tableName+
									" WHERE "+fixedPrimaryKeyName+" IN ("+listToString(vdpElements)+")");
							//forces fetching the full content of the ResultSet at once, holding it all in memory 
							List<Row> rows = queryResults.all();
							int actualEntitiesNumber = rows.size();
							
							//
							//transformation to the metamodel for each row
							//
							for(Row row : rows){
								//create a new cassandra model instance
								CassandraModel cassModel = new CassandraModel();
								//set the table
								cassModel.setTable(tableName);
								//set the key
								//the primary key name has to be set by default (see limitations)
								
								//FIXME - General solution
								TableMetadata tableMetadata = cluster.getMetadata().getKeyspace(keySpace).getTable(tableName);
								List<ColumnMetadata> pks = tableMetadata.getPrimaryKey();
								//values composing the PK should be converted into a single string!
								String pkStr = CassandraTypesUtils.primaryKeyToString(pks, row);
								cassModel.setKeyValue(pkStr);
								//--------------------------------------
								
								//
								//COLUMNS
								//
								Iterator<ColumnDefinitions.Definition> columnsIterator=row.getColumnDefinitions().iterator();
								while(columnsIterator.hasNext()){
									ColumnDefinitions.Definition column=columnsIterator.next();
									
									//don't add the fixed primary key (id) column
									if(!column.getName().equals(fixedPrimaryKeyName)){
										CassandraColumn cassColumn=new CassandraColumn();
										
										//set name
										String columnName=column.getName();
										cassColumn.setColumnName(columnName);
										try{
											//set type + value 
											String dataType=column.getType().toString();
											//log.debug("Column {} type {}", columnName, dataType);
											setValueAndType(cassColumn,columnName,dataType,row);
											//set indexed 
											//if(tableMetadata.getColumn(columnName).getIndex()!=null){
											if(tableMetadata.getIndex(columnName)!=null){
												cassColumn.setIndexed(true);
											}else{
												cassColumn.setIndexed(false);
											}
											//add to the cassandra model
											cassModel.addColumn(cassColumn);
											
										}catch(ClassNotFoundException ex){
											log.error(Thread.currentThread().getName() + " - Error managing column: "+column.getName()
													+" for row: "+pkStr
													+" on cassandra table: "+tableName, ex);
										}
									}
								}
								//from cassandraModel to MetaModel
								Metamodel meta=cassandraTransformer.toMyModel(cassModel);
								
								//Piggybacking the actual number of entities the TWC should expect.
								HashMap<String, Integer> counters = new HashMap<String, Integer>();
								counters.put(tableName, actualEntitiesNumber);
								meta.setActualVdpSize(counters);
								
								if(meta!=null && !stopThreads){
									try{
										//serialize & add to the queue
										taskQueues.get(thread_id).publish(serializer.serialize(meta));
									}catch (QueueException e) {
										log.error(Thread.currentThread().getName() + " - Error communicating with the queue " + 
												TaskQueue.getDefaultTaskQueueName(), e);
									} catch (TException e) {
										log.error(Thread.currentThread().getName() + " - Error serializing message ", e);
									}
								}else{
									log.warn(Thread.currentThread().getName() + 
											" - Couldn't convert entity to metamodel or SRTs were stopped.");
								}
						  }
						
						}else{
							log.debug(Thread.currentThread().getName()+
									" Skipping VDP with id "+VDPid);
						}
					}else{
						//log.debug(Thread.currentThread().getName()+
						//		" Pre-Skipping VDP with id "+VDPid);
					}
				} catch (FailedLockException e) {
					log.error("{} - ZK error when trying to acquire lock! Retrying with VDP {}",
							Thread.currentThread().getName(),
							VDPid--);
        			} catch (NoHostAvailableException e) { 
        				Map<InetSocketAddress, Throwable> errors = e.getErrors();
        				Set<InetSocketAddress> keySet = errors.keySet();
        				for(InetSocketAddress k : keySet){
        					log.error(k+": "+errors.get(k).getMessage()+
        							"\n\tCause: "+errors.get(k).getCause());	
        				}
        				log.error(Thread.currentThread().getName() +
    							" Cassandra throw error processing table: "+tableName+
    							".\nMessage: "+e.getMessage()+
    							".\nStackTrace: "+e.getStackTrace(), e);
        				return null;
				} catch (Exception e) {
					log.error(Thread.currentThread().getName() +
							" Error setting the initial migration status for table: "+tableName, e);
    					return null;
				}
				
				//getting VDPid from the shared VDP priority queue
    			try {
					VDPid = vdpsQueue.poll(tableName, 1, TimeUnit.MINUTES);
				} catch (InterruptedException e) {
					VDPid = null;
					log.error("{} - VDPs extraction was interrupted. Stack Trace:\n{}", Thread.currentThread().getName(),e);
				}
			}while(VDPid != null && stopThreads == false);
			
			if(stopThreads){
        		log.info("{} - Received Stop Thread Request!",Thread.currentThread().getName());
        		break;
			}
	        //Finish all assigned vdps for this kind
	        log.debug(Thread.currentThread().getName()+" ==> Transferred "+i+" entities from table "+tableName);
		}
		
		return null;
	}

	private static <E> String listToString(List<E> list){
		String keyType = ConfigurationManagerCassandra.getConfigurationProperties("cassandra.primarKey.type");
		boolean isText=false;
		if(keyType!=null && (keyType.toLowerCase().equals("string")||keyType.toLowerCase().equals("text"))){
			isText=true;
		}
		if(list==null) return "";
		int size = list.size();
		if(size>0){
			StringBuilder sb = new StringBuilder();
			int counter = 1;
			for(E e : list){
				if(isText) sb.append("'");
				sb.append(e);
				if(isText) sb.append("'");
				if(counter<size)
					sb.append(",");
				counter++;
			}
			return sb.toString();
		}
		return "";
	}
	
	private Collection<String> filter(Collection<String> fullList){
		   List<String> excluded = PropertiesManager.getExcludedTables();
		   List<String> included = PropertiesManager.getIncludedTables();
		   
		   if(excluded.isEmpty() && included.isEmpty()){
			   return fullList;
		   }
		   
		   //removing elements from the exclusion list that should be explicitly allowed
		   excluded.removeAll(included);
		   
		   Collection<String> newList = fullList;
		   
		   if(!excluded.isEmpty()){
			   //blacklisting
			   newList.remove(excluded);
		   }
		   
		   if(!included.isEmpty()){
			   //whitelisting
			   newList.retainAll(included);
		   }
		   
		   return newList;
	   }
	
	@Override
	public List<String> getTableList() {
		if(isConnected()){
			int thread_id = 0;
			Session session=connectionList.get(thread_id).session;
			Cluster cluster=session.getCluster();
			String keySpace=ConfigurationManagerCassandra.getConfigurationProperties(Constants.KEYSPACE);
			Collection<TableMetadata> tables=cluster
					.getMetadata()
					.getKeyspace(keySpace)
					.getTables();
			
			ArrayList<String> tablesNames = new ArrayList<String>(tables.size());
			for(TableMetadata table : tables){
				tablesNames.add(table.getName());
			}
			return tablesNames;
		}
		return null;
	}

}
