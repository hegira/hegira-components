package it.polimi.hegira.schedulers;

import it.polimi.hegira.adapters.AbstractDatabase;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class StopScheduler {
	private static Logger log = LoggerFactory.getLogger(StopScheduler.class);
	private final ScheduledExecutorService scheduler;
	private long delay = -1;
	private TimeUnit unit;
	private AbstractDatabase db;
	private ScheduledFuture<?> schedule;
	
	/**
	 * Creates a new instance of the {@link StopScheduler} class 
	 * that stops the migration threads (SRTs or TWTs) after a given delay. 
	 * @param delay the time from now to delay execution
	 * @param unit the time unit of the delay parameter
	 * @param the instance of the database adapter whose threads should be stopped
	 */
	public StopScheduler(long delay, TimeUnit unit, AbstractDatabase db){
		this.scheduler = Executors.newScheduledThreadPool(1);
		this.delay = delay;
		this.unit = unit;
		this.db = db;
	}
	
	/**
	 * Creates and executes a one-shot stop action that becomes enabled after the given delay.
	 */
	public void scheduleStop(){
		if(scheduler == null || delay < 0 || unit == null || db == null){
			log.error("{} - StopScheduler class wasn't properly instantiated!", 
					Thread.currentThread().getName());
			return;
		}
		
		final Runnable stopRunnable = new Runnable() {
			@Override
			public void run() {
				log.info("{} - Stopping adapters' threads",
						Thread.currentThread().getName());
				db.stopThreads();
			}
		};
		
		log.info("{} - The component's threads will be stopped in {} {}",
						Thread.currentThread().getName(),
						delay, unit.name());
		schedule = scheduler.schedule(stopRunnable, delay, unit);
	}
	
	/**
	 * Attempts to cancel execution of a stop task. This attempt will fail if the task has already completed, 
	 * has already been cancelled, or could not be cancelled for some other reason. 
	 * @return false if the task could not be cancelled, 
	 * typically because it has already completed normally (or it was not set); true otherwise
	 */
	public boolean cancelScheduledStop(){
		if(schedule!=null)
			return schedule.cancel(true);
		else
			return false;
	}

	/**
	 * @param delay the delay to set
	 */
	public synchronized void setDelay(long delay) {
		this.delay = delay;
	}

	/**
	 * @param unit the unit to set
	 */
	public synchronized void setUnit(TimeUnit unit) {
		this.unit = unit;
	}

	/**
	 * @param db the db to set
	 */
	public synchronized void setDb(AbstractDatabase db) {
		this.db = db;
	}
}
