package it.polimi.hegira.schedulers;

import it.polimi.hegira.adapters.AbstractDatabase;
import it.polimi.hegira.exceptions.InitializationException;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class SamplerScheduler {
	private static Logger log = LoggerFactory.getLogger(SamplerScheduler.class);
	
	private final ScheduledExecutorService scheduler;
	private ScheduledFuture<?> schedule;
	
	private long initialDelay = 0;
	private long samplingPeriod;
	private TimeUnit unit;
	private AbstractDatabase db;
	private SamplingRunnable sRunnable;
	
	/**
	 * Creates a new instance of the  {@link SamplerScheduler} class 
	 * that will sample the number of migrated entities.
	 * @param initialDelay the time to delay first execution
	 * @param samplingPeriod the period between successive executions
	 * @param unit the time unit of the initialDelay and period parameters
	 * @param db the instance of the target database adapter
	 * @throws InitializationException 
	 */
	public SamplerScheduler(long initialDelay, long samplingPeriod, TimeUnit unit, AbstractDatabase db, String expName) throws InitializationException{
		this.scheduler = Executors.newScheduledThreadPool(1);
		this.initialDelay = initialDelay;
		this.samplingPeriod = samplingPeriod;
		this.unit = unit;
		this.db = db;
		this.sRunnable = SamplingRunnable.init(expName, db).getInstance();
	}
	
	public void startSampling() {
		if(scheduler == null || initialDelay < 0 || samplingPeriod < 1 || 
				unit == null || db == null || sRunnable == null){
			log.error("{} - SamplerScheduler class wasn't properly instantiated!", 
					Thread.currentThread().getName());
			return;
		}
		
		log.info("{} - Migrated entities sampling will start in {} {}. Sampling period {} {}.",
				Thread.currentThread().getName(),
				initialDelay, unit.name(),
				samplingPeriod, unit.name());
		this.schedule = scheduler.scheduleAtFixedRate(sRunnable, 
				initialDelay, 
				samplingPeriod, 
				unit);
	}
	
	public boolean stopSampling(){
		if(schedule!=null)
			return schedule.cancel(true);
		else
			return false;
	}
	
	private void restartSampling() {
		if(stopSampling()){
			log.info("{} - Restarting sampling.", 
					Thread.currentThread().getName());
			startSampling();
		}
	}

	/**
	 * @param initialDelay the initialDelay to set
	 * @throws InitializationException 
	 */
	public synchronized void updateInitialDelay(long initialDelay) {
		this.initialDelay = initialDelay;
		restartSampling();
	}

	/**
	 * @param samplingPeriod the samplingPeriod to set
	 * @throws InitializationException 
	 */
	public synchronized void updateSamplingPeriod(long samplingPeriod) {
		this.samplingPeriod = samplingPeriod;
		restartSampling();
	}

	/**
	 * @param unit the unit to set
	 * @throws InitializationException 
	 */
	public synchronized void updateUnit(TimeUnit unit) {
		this.unit = unit;
		restartSampling();
	}

	
}
