package it.polimi.hegira.schedulers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.polimi.hegira.adapters.AbstractDatabase;
import it.polimi.hegira.exceptions.InitializationException;

/**
 * @author Marco Scavuzzo
 *
 */
public class SamplingRunnable implements Runnable {
	private static Logger log = LoggerFactory.getLogger(SamplingRunnable.class);
	
	private static String expName;
	private static AbstractDatabase db;
	
	private static String filePath;
	private Long previousTotal = 0L;
	private static SamplingRunnable instance = new SamplingRunnable();
	
	public static synchronized SamplingRunnable init(String name, AbstractDatabase database) {
		if(expName==null && name != null){
			expName = name;
			filePath = System.getProperty("user.home")+File.separator+
					"hegira"+File.separator+
					"components"+File.separator+
					expName+"-samples.txt";
		}
		if(db == null && database != null) db = database;
		
		return instance;
	}

	public SamplingRunnable getInstance() throws InitializationException {
		//if the user didn't initialize the class
		if(expName == null || db == null)
			throw new InitializationException("Must first be initialized!");
		
		return instance;
	}
	
	@Override
	public void run() {
		if(db.isStopped()) return;
		
		long timestamp = System.currentTimeMillis();
		long total = db.getVDPsCountersTotal();
		appendSample(timestamp, total-previousTotal);
		
		synchronized (previousTotal) {
			previousTotal = total;
		}	
	}
	
	private void appendSample(long timestamp, long difference) {

		if(filePath==null){
			log.error("{} - Error when trying to write file: {}",
					Thread.currentThread().getName(),
					filePath);
			return;
		}
		BufferedWriter bw = null;
		try {
			File file = new File(filePath);
			if (!file.getParentFile().exists())
			    file.getParentFile().mkdirs();
			if (!file.exists())
			    file.createNewFile();
			bw = new BufferedWriter(new FileWriter(file,true));
			bw.write(expName+" "+timestamp+" "+difference);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			log.error("{} - Error when trying to write file: {}",
					Thread.currentThread().getName(),
					filePath);
			return;
		}finally{
			if(bw!=null){
				try {
					bw.close();
				} catch (IOException e) {
					log.error("{} - Failed to close the BufferedWriter:\n",
							Thread.currentThread().getName(),
							e);
				}
			}
		}
	
	}


}
