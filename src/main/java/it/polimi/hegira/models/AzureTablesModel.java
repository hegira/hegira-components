/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.microsoft.windowsazure.services.table.client.DynamicTableEntity;
import com.microsoft.windowsazure.services.table.client.EntityProperty;

import it.polimi.hegira.transformers.AzureTablesTransformer;
import it.polimi.hegira.utils.DefaultSerializer;
import it.polimi.hegira.utils.TestsUtils;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * @author Marco Scavuzzo
 *
 */
public class AzureTablesModel {
	private String tableName;
	private DynamicTableEntity entity;
	//Entities contained by the same table
	private List<DynamicTableEntity> entities;
	
	public AzureTablesModel(){
		
	}
	
	public AzureTablesModel(String tableName, DynamicTableEntity entity){
		this.tableName = tableName;
		this.entity = entity;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public DynamicTableEntity getEntity() {
		return entity;
	}

	public void setEntity(DynamicTableEntity entity) {
		this.entity = entity;
	}

	public List<DynamicTableEntity> getEntities() {
		return entities;
	}

	public void setEntities(List<DynamicTableEntity> entities) {
		this.entities = entities;
	}
	
	
	private HashMap<String,DynamicTableEntity> toMap(List<DynamicTableEntity> list){
		HashMap<String, DynamicTableEntity> map = new HashMap<String,DynamicTableEntity>();
		for(DynamicTableEntity e : list){
			map.put(e.getRowKey(), e);
		}
		return map;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if(!(obj instanceof AzureTablesModel))
			return false;
		AzureTablesModel inst = (AzureTablesModel) obj;
		
		if(this.tableName!=null && inst.tableName!=null && !this.tableName.equals(inst.tableName))
			return false;
		
		HashMap<String,DynamicTableEntity> t_entities = toMap(this.getEntities());
		HashMap<String,DynamicTableEntity> o_entities = toMap(inst.getEntities());
		
		
		
		if(t_entities!=null && o_entities!=null){
			if(t_entities.size()!=o_entities.size())
				return false;
			
			for(Map.Entry<String, DynamicTableEntity> t_e : t_entities.entrySet()){
				DynamicTableEntity o_e = o_entities.get(t_e.getKey());
				
				
				String t_pk = t_e.getValue().getPartitionKey();
				//if(!t_pk.equals(o_e.getPartitionKey()))
				//	return false;
				String t_rk = t_e.getValue().getRowKey();
				if(!t_rk.equals(o_e.getRowKey()))
					return false;
				String t_ts = t_e.getValue().getTimestamp().toString();
				if(!t_ts.equals(o_e.getTimestamp().toString()))
					return false;
				
				HashMap<String, EntityProperty> t_props = t_e.getValue().getProperties();
				HashMap<String, EntityProperty> o_props = o_e.getProperties();
				if(t_props.size()!=o_props.size())
					return false;
				//choosing t_props as pivot
				for(Map.Entry<String, EntityProperty> t_entry : t_props.entrySet()){
					EntityProperty o_entry = o_props.get(t_entry.getKey());
					if(o_entry==null)
						return false;
					
					if(!t_entry.getValue().getEdmType().toString().equals(o_entry.getEdmType().toString()))
						return false;
					
					
					try {
						if(!Arrays.equals(getBytesFromProperty(t_entry.getValue()), 
								getBytesFromProperty(o_entry)))
							return false;
					} catch (IOException e) {
						return false;
					}
				}
			}
		}else{
			//We consider two azuretablesmodels having null entities to be different.
			return false;
		}
		
		return true;
	}
	
	private byte[] getBytesFromProperty(EntityProperty prop) throws IOException{
		String type = prop.getEdmType().name();
		//System.out.println("type: "+type);
		switch(type){
			case "BINARY":
				return prop.getValueAsByteArray();
	
			case "BOOLEAN":
				return DefaultSerializer.serialize(prop.getValueAsBoolean());
				
			case "DATETIME":
				return DefaultSerializer.serialize(prop.getValueAsDate());
				
			case "DOUBLE":
				return DefaultSerializer.serialize(prop.getValueAsDouble());
				
			case "GUID":
				return DefaultSerializer.serialize(prop.getValueAsUUID());
				
			case "INT32":
				return DefaultSerializer.serialize(prop.getValueAsInteger());
				
			case "INT64":
				return DefaultSerializer.serialize(prop.getValueAsLong());
				
			case "STRING":
				return DefaultSerializer.serialize(prop.getValueAsString());
				
			default:
				throw new IOException("Can't serialize object.");
		}
	}
	
	public static void main(String[] args) throws IOException {
		
		Metamodel mm = TestsUtils.getMetamodelTestingEntity();
		
		AzureTablesModel e1 = new AzureTablesTransformer().fromMyModel(mm);
		AzureTablesModel e2 = new AzureTablesTransformer().fromMyModel(mm);
		
		System.out.println(e1.equals(e2)?"Equal":"Not equal!");
	}
}
