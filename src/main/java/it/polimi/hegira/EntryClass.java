/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira;

import it.polimi.hegira.adapters.AbstractDatabase;
import it.polimi.hegira.adapters.DatabaseFactory;
import it.polimi.hegira.exceptions.InitializationException;
import it.polimi.hegira.exceptions.QueueException;
import it.polimi.hegira.queue.ServiceQueue;
import it.polimi.hegira.queue.ServiceQueueMessage;
import it.polimi.hegira.schedulers.SamplerScheduler;
import it.polimi.hegira.schedulers.StopScheduler;
import it.polimi.hegira.utils.CLI;
import it.polimi.hegira.utils.CLI.TestsCommand;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;
import it.polimi.hegira.utils.PropertiesManager;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.JCommander;

/**
 * Hegira-components entry point.
 * @author Marco Scavuzzo
 */
public class EntryClass {
	private static Logger log = LoggerFactory.getLogger(EntryClass.class);
	private static String command;
	private static TestsCommand tc;
	private static StopScheduler stopScheduler;
	private static SamplerScheduler samplerScheduler;
	
	public static void main(String[] args) {
		//PropertyConfigurator.configure(Thread.currentThread().getContextClassLoader().getResource("log.properties").getFile());
		
		CLI cli = new CLI();
		JCommander jc = new JCommander(cli);
		jc.setProgramName("Hegira components");
		
		tc = cli.new TestsCommand();
		jc.addCommand(TestsCommand.commandName, tc);
		
		try{
			//jc = new JCommander(cli,args);
			jc.parse(args);
			command = jc.getParsedCommand();
			if(command!=null){
				switch(command){
					case TestsCommand.commandName:
						break;
					default: 
						jc.usage();
						return;
				}
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
			if(jc.getParsedCommand()!=null){
				jc.usage(jc.getParsedCommand());
			} else {
				jc.usage();
			}
			return;
		}
			
		ServiceQueue serviceQueue = new ServiceQueue(cli.componentType, cli.queueAddress);
		try {
			//Telling hegira-api that we are ready to receive commands
			try{
				serviceQueue.announcePresence();
			}catch(QueueException | NullPointerException ex){
				log.error("Unable to connect to the Queue. The program threw the following exception: ", ex);
				return;
			}
			
			AbstractDatabase src = null;
			AbstractDatabase dst = null;
			
			/**
			 * Continuously waiting for command messages.
			 * Actions to message should be executed by threads, 
			 * in order for the application to be responsive (to other commands, 
			 * 	i.e stop-migration, report-status, ecc..). 
			 * TODO: Currently multiple concurrent migrations are not supported, but this is not checked!
			 */
			while(true){
				
				ServiceQueueMessage sqm = serviceQueue.receiveCommands();
				
				switch(sqm.getCommand()){
					case "switchover":
						if(cli.componentType.equals("SRC")){
							log.debug("Received command message, destined to: SRC");
							HashMap<String, String> options_producer = new HashMap<String,String>();
							options_producer.put("mode", Constants.PRODUCER);
							if(sqm.getSRTs_NO()>=1)
								options_producer.put("SRTs_NO", ""+sqm.getSRTs_NO());
							
							options_producer.put("queue-address", cli.queueAddress);
							options_producer.put("zkServer.version", getZkServerVersion());
							
							src = DatabaseFactory.getDatabase(sqm.getSource(), options_producer);
							src.switchOver("SRC");
		            	}else if(cli.componentType.equals("TWC")){
	            			log.debug("Received command message, destined to: TWC");
	            			HashMap<String, String> options_consumer = new HashMap<String,String>();
	        				options_consumer.put("mode", Constants.CONSUMER);
	        				if(sqm.getThreads()>=1){
	        					options_consumer.put("threads", ""+sqm.getThreads());
	        				}
	        				
	        				options_consumer.put("queue-address", cli.queueAddress);
	        				options_consumer.put("zkServer.version", getZkServerVersion());
	        				dst = DatabaseFactory.getDatabase(sqm.getDestination().get(0),
	        						options_consumer);
	        				dst.switchOver("TWC");
		            	}
						
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						break;
					case "switchoverPartitioned":
						if(cli.componentType.equals("SRC")){
							log.debug("Received command message, destined to: SRC");
							HashMap<String, String> options_producer = new HashMap<String,String>();
							options_producer.put("mode", Constants.PRODUCER);
							options_producer.put("queue-address", cli.queueAddress);
							options_producer.put("SRTs_NO", ""+sqm.getSRTs_NO());
							options_producer.put("zkServer.version", getZkServerVersion());
							
							src = DatabaseFactory.getDatabase(sqm.getSource(), options_producer);
							
							//Start the scheduler
							if(command!=null && command.equals(TestsCommand.commandName) && tc!=null){
								stopScheduler = 
										new StopScheduler(tc.stopDelay, tc.parseTimeUnit(tc.stopDelayUnit), src);
								stopScheduler.scheduleStop();
							}
							src.switchOverPartitioned("SRC", false);
		            	}else if(cli.componentType.equals("TWC")){
	            			log.debug("Received command message, destined to: TWC");
	            			HashMap<String, String> options_consumer = new HashMap<String,String>();
	        				options_consumer.put("mode", Constants.CONSUMER);
	        				if(sqm.getThreads()>=1){
	        					options_consumer.put("threads", ""+sqm.getThreads());
	        				}
	        				options_consumer.put("queue-address", cli.queueAddress);
	        				options_consumer.put("zkServer.version", getZkServerVersion());
	        				
	        				dst = DatabaseFactory.getDatabase(sqm.getDestination().get(0),
	        						options_consumer);
		        				
		        			//Start the schedulers
							if(command!=null && command.equals(TestsCommand.commandName) && tc!=null){
								stopScheduler = 
										new StopScheduler(tc.stopDelay, tc.parseTimeUnit(tc.stopDelayUnit), dst);
								stopScheduler.scheduleStop();
								
								if(tc.samplingPeriod>0){
									try {
										samplerScheduler = 
												new SamplerScheduler(tc.initialDelay, tc.samplingPeriod, 
														tc.parseTimeUnit(tc.sampleUnit), dst, tc.expName);
										samplerScheduler.startSampling();
									} catch (InitializationException e) {
										log.error("{} - Sampler Scheduler initialization exception",
												Thread.currentThread().getName(),
												e);
										return;
									}
								}
							}
		        			dst.switchOverPartitioned("TWC",false);
		            	}
						
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						break;
						
					case "recover":
						if(cli.componentType.equals("SRC")){
							//Reads the MigrationStatus from ZooKeeper and rebuilds a local status from which to start migrating
							log.debug("Received command message, destined to: SRC");
							HashMap<String, String> options_producer = new HashMap<String,String>();
							options_producer.put("mode", Constants.PRODUCER);
							options_producer.put("queue-address", cli.queueAddress);
							options_producer.put("zkServer.version", getZkServerVersion());
							
							src = DatabaseFactory.getDatabase(sqm.getSource(), options_producer);
							
							//Start the scheduler
							if(command!=null && command.equals(TestsCommand.commandName) && tc!=null){
								stopScheduler = 
										new StopScheduler(tc.stopDelay, tc.parseTimeUnit(tc.stopDelayUnit), src);
								stopScheduler.scheduleStop();
							}
							src.switchOverPartitioned("SRC", true);
						}else if(cli.componentType.equals("TWC")){
							//shouldn't be necessary to adjust the counters for the entities correctly migrated (expected 
							//number is piggybacked in the metamodel entity) since an already started VDP is recovered anyway
							
							log.debug("Received command message, destined to: TWC");
		            			HashMap<String, String> options_consumer = new HashMap<String,String>();
		        				options_consumer.put("mode", Constants.CONSUMER);
		        				if(sqm.getThreads()>=1){
		        					options_consumer.put("threads", ""+sqm.getThreads());
		        				}
		        				options_consumer.put("queue-address", cli.queueAddress);
		        				options_consumer.put("zkServer.version", getZkServerVersion());
		        				
		        				dst = DatabaseFactory.getDatabase(sqm.getDestination().get(0),
		        						options_consumer);
		        				
		        			//Start the schedulers
							if(command!=null && command.equals(TestsCommand.commandName) && tc!=null){
								stopScheduler = 
										new StopScheduler(tc.stopDelay, tc.parseTimeUnit(tc.stopDelayUnit), dst);
								stopScheduler.scheduleStop();
								
								if(tc.samplingPeriod>0){
									try {
										samplerScheduler = 
												new SamplerScheduler(tc.initialDelay, tc.samplingPeriod, 
														tc.parseTimeUnit(tc.sampleUnit), dst, tc.expName);
										samplerScheduler.startSampling();
									} catch (InitializationException e) {
										log.error("{} - Sampler Scheduler initialization exception",
												Thread.currentThread().getName(),
												e);
										return;
									}
								}
							}
		        				dst.switchOverPartitioned("TWC",true);
						}
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						break;
					case "pause-start":
						handlePauseStart(cli, src, dst, sqm);
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						log.debug(Thread.currentThread()+
								" - Announcing presence ");
						break;
					case "pause-end":
						log.debug(Thread.currentThread()+
								" - Received command message: "+sqm.getCommand());
						handlePauseEnd(cli, src, dst);
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						break;
					case "slowdown-start":
						handleSlowDownStart(cli, src, dst, sqm);
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						break;
					case "slowdown-end":
						handleSlowDownEnd(cli, src, dst);
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						break;
					case "revokeStop":
						if(stopScheduler!=null)
							stopScheduler.cancelScheduledStop();
						break;
					case "stopSampling":
						if(samplerScheduler!=null)
							samplerScheduler.stopSampling();
						break;
					default:
						log.debug("Received command message: "+sqm.getCommand());
						//Telling hegira-api that we are ready to receive other commands
						serviceQueue.announcePresence();
						break;
				}
			}
		} catch (QueueException e) {
			log.error(DefaultErrors.queueError);
			log.error(e.getMessage());
			return;
		}
	}
	
	/**
	 * Upon receiving a request to pause the data migration, this method notifies the proper component of such request.
	 * @param cli The command line parameters used to launch the component.
	 * @param src Reference to the source database.
	 * @param dst Reference to the target database.
	 * @param sqm The message retrieved from the {@link ServiceQueue} containing the operation to perform (i.e. pause).
	 */
	private static void handlePauseStart(CLI cli, AbstractDatabase src, AbstractDatabase dst, ServiceQueueMessage sqm){
		Long timeout = (sqm != null && sqm.getTimeout() <= 0L) ? null : sqm.getTimeout();
		
		if(cli.componentType.equals("SRC") && src!=null){
			src.getPauseSource().notifyPauseRequest(timeout);
		}else if(cli.componentType.equals("TWC") & dst!=null){
			dst.getPauseSource().notifyPauseRequest(timeout);
		}else{
			log.error(Thread.currentThread().getName()+
					" - Cannot handle the pause event!");
		}
	}
	
	/**
	 * Upon receiving a request to slow-down the data migration, this method notifies the proper component of such request.
	 * @param cli The command line parameters used to launch the component.
	 * @param src Reference to the source database.
	 * @param dst Reference to the target database.
	 * @param sqm The message retrieved from the {@link ServiceQueue} containing the operation to perform (i.e., slow-down).
	 */
	private static void handleSlowDownStart(CLI cli, AbstractDatabase src, AbstractDatabase dst, ServiceQueueMessage sqm){
		Long timeout = (sqm != null && sqm.getTimeout() > 0) ? sqm.getTimeout() : null;
		if(cli.componentType.equals("SRC") && src!=null){
			src.getSlowdownSource().notifySlowDownRequest(timeout, sqm.getPausesDuration());
		}else if(cli.componentType.equals("TWC") & dst!=null){
			dst.getSlowdownSource().notifySlowDownRequest(timeout, sqm.getPausesDuration());
		}else{
			log.error(Thread.currentThread().getName()+
					" - Cannot handle the SlowDown event!");
		}
	}
	
	/**
	 * Upon receiving a request to stop a previously started data migration pause,
	 * this method notifies the proper component of such request.
	 * @param cli The command line parameters used to launch the component.
	 * @param src Reference to the source database.
	 * @param dst Reference to the target database.
	 */
	private static void handlePauseEnd(CLI cli, AbstractDatabase src, AbstractDatabase dst){
		if(cli.componentType.equals("SRC") && src!=null){
			src.getPauseSource().notifyPauseTermination();
		}else if(cli.componentType.equals("TWC") & dst!=null){
			dst.getPauseSource().notifyPauseTermination();
		}else{
			log.error(Thread.currentThread().getName()+
					" - Cannot handle the pause interruption event!");
		}
	}
	
	/**
	 * Upon receiving a request to stop a previously started data migration slow-down,
	 * this method notifies the proper component of such request.
	 * @param cli The command line parameters used to launch the component.
	 * @param src Reference to the source database.
	 * @param dst Reference to the target database.
	 */
	private static void handleSlowDownEnd(CLI cli, AbstractDatabase src, AbstractDatabase dst){
		if(cli.componentType.equals("SRC") && src!=null){
			src.getSlowdownSource().notifySlowDownTermination();
		}else if(cli.componentType.equals("TWC") & dst!=null){
			dst.getSlowdownSource().notifySlowDownTermination();
		}else{
			log.error(Thread.currentThread().getName()+
					" - Cannot handle the SlowDown interruption event!");
		}
	}
	
	private static String getZkServerVersion(){
		return PropertiesManager.getCredentials("zkServer.version");
	}
}