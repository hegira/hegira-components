package it.polimi.hegira.coordination;

import it.polimi.hegira.exceptions.InitializationException;
import it.polimi.hegira.utils.PropertiesManager;
import it.polimi.hegira.vdp.VdpUtils;
import it.polimi.hegira.zkWrapper.MigrationStatus;
import it.polimi.hegira.zkWrapper.VDPmigrationStatus;
import it.polimi.hegira.zkWrapper.ZKserver;
import it.polimi.hegira.zkWrapper.ZKserver2;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;
import it.polimi.hegira.zkWrapper.statemachine.StateMachine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZooKeeper {
	private static final transient Logger log = LoggerFactory.getLogger(ZooKeeper.class);
	private static String connectString;
	private static ZKserver zk;
	private static ZKserver2 zk2;
	private static boolean useZkServer2=false;
	private static Integer VDPsize=-1;
	private HashMap<String, Integer> tablesSeqNrs;
	
	private static ZooKeeper instance = new ZooKeeper();

	
	public ZooKeeper() {
		tablesSeqNrs = new HashMap<String, Integer>(); 
	}
	
	/**
	 * Initializes {@link ZooKeeper} class by creating a stable connection
	 * to the given ZooKeeper connection string.
	 * @param cs ZooKeeper connection string ip:port
	 * @return The {@link ZooKeeper} class instance
	 */
	public static synchronized ZooKeeper init(String cs){
		if(connectString==null && cs!=null){
			String zkVersionStr = PropertiesManager.getCredentials("zkServer.version");
			if(zkVersionStr!=null && !zkVersionStr.isEmpty()){
				try{
					int zkServerVersion = Integer.parseInt(zkVersionStr);
					if(zkServerVersion>1){
						useZkServer2=true;
					}
				}catch(Exception e){}
			}
			connectString = cs;
			if(!useZkServer2 && zk==null)
				connect();
			else if(useZkServer2 && zk2==null)
				connect();
		}	
		return instance;
	}
	
	public static synchronized ZooKeeper reinit(String cs){
		if(connectString!=null){
			if(cs!=null){
				connectString = cs;
				if(!useZkServer2 && zk!=null){
					disconnect();
				}else if(useZkServer2 && zk2!=null){
					disconnect();
				}
				connect();
			}else{
				log.error("Provide a valid connection string");
			}
		}else{
			log.error("Never initilized");
		}
		return instance;
	}

	private static synchronized void connect(){
		if(connectString!=null){
			if(!useZkServer2 && zk==null){
				log.debug("Connecting...");
				zk = new ZKserver(connectString);
			}else if(useZkServer2 && zk2==null){
				log.debug("Connecting...");
				zk2 = new ZKserver2(connectString);
			}
		}
	}
	
	public static ZooKeeper getInstance() throws InitializationException{
		//if the user didn't initialize the class
		if(connectString==null)
			throw new InitializationException("Must first be initialized!");
		
		if(zk!=null || zk2!=null){
			//if already connected
			return instance;
		}else {
			//else if not connected, let's connect
			connect();
			return instance;
		}
	}
	
	public List<String> getTablesList(){
		List<String> tables = null;
		try {
			if(!useZkServer2 && zk!=null)
				tables = zk.getMigrationPaths();
			else if(useZkServer2 && zk2!=null)
				tables = zk2.getMigrationPaths();
			else	
				log.error("Disconnected?");
		} catch (Exception e) {
			log.error("Couldn't get the list of tables! Have you started a migration?");
		}
		return tables!=null ? tables : new ArrayList<String>();
	}
	
	public static synchronized void disconnect(){
		if(zk!=null || zk2!=null){
			if(!useZkServer2){
				zk.close();
				zk=null;
			}else if(useZkServer2){
				zk2.close();
				zk2=null;
			}
			log.debug("Disconnected");
		}else{
			log.debug("Already disconnected");
		}
		
	}
	
	private MigrationStatus getTableMigrationStatus(String tableName){
		try {
			MigrationStatus ms = zk.getFreshMigrationStatus(tableName, null);
			return ms;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private VDPmigrationStatus getVdpMigrationStatus(String tableName, String vdpId){
		try {
			VDPmigrationStatus vdpMs = zk2.getFreshMigrationStatus(tableName, vdpId, null);
			return vdpMs;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			log.warn("{} - Error deserializing the VDPmigrationStatus");
		}
		return null;
	}
	
	public State getVDPstatus(String tableName, int VDPid) throws OutOfSnapshotException{
		if(tableName == null || VDPid < 0) return null;
		StateMachine smStatus=null;
		if(!useZkServer2){
			MigrationStatus ms = getTableMigrationStatus(tableName);
			smStatus = ms.getVDPstatus(VDPid);
		}else {
			VDPmigrationStatus vdpMs = getVdpMigrationStatus(tableName, VDPid+"");
			if(vdpMs!=null)
				smStatus = vdpMs.getVDPstatus();
		}
		if(smStatus!=null){
			State currentState = smStatus.getCurrentState();
			return currentState;
		}else
			return null;
	}
	
	public int getTableVDPsNo(String tableName){
		if(tableName == null) return 0;
		if(!useZkServer2){
			MigrationStatus ms = getTableMigrationStatus(tableName);
			return ms.getTotalVDPs(getVDPsize(false));
		}else{
			
			try {
				int lastsqnr=getTableLastSeqNr(tableName);
				int vdpsize = getVDPsize(false);
				if(vdpsize<0)
					return -1;
				int totalvdps = VdpUtils.getTotalVDPs(lastsqnr, vdpsize);
				return totalvdps;
			} catch (Exception e) {
				log.error("{} - Cannot calculate the total number of VDPs for table {} (zkServer.version 2)",
						Thread.currentThread().getName(),
						tableName);
				return -1;
			}
		}
	}
	
	public int getTableLastSeqNr(String tableName){
		if(tableName == null) return 0;
		if(tablesSeqNrs.get(tableName)!=null){
			return tablesSeqNrs.get(tableName);
		}
		
		int value;
		if(!useZkServer2){
			MigrationStatus ms = getTableMigrationStatus(tableName);
			value = ms.getLastSeqNr();
		}else{
			value = zk2.getCurrentSeqNr(tableName);
		}
		
		tablesSeqNrs.put(tableName, value);
		return value;
	}
	
	/**
	 * Retrieves the size of all VDPs from ZooKeeper.
	 * I.e., 10^e
	 * @param force VDPsize cached value is possibly refreshed.
	 * @return The exponent e
	 */
	public int getVDPsize(boolean force){
		if(force || VDPsize<0){
			try {
				if(!useZkServer2 && zk!=null && !zk.isLocked(null)){
					synchronized (VDPsize) {
						VDPsize = zk.getVDPsize();
					}
				}else if(useZkServer2 && zk2!=null && !zk2.isLocked(null)){
					synchronized (VDPsize) {
						VDPsize = zk2.getVDPsize();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return VDPsize.intValue();
	}
}
