package it.polimi.hegira.coordination;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VDPsCheckerController {
	private static final transient Logger log = LoggerFactory.getLogger(VDPsCheckerController.class);
	private VDPsCheckerRunnable runnable;
	private Thread runnableThread;
	
	/**
	 * 
	 * @param zk Initialized ZooKeeper instance
	 * @param vdpsTables Key = tableName Value = lastVDP
	 */
	public VDPsCheckerController(ZooKeeper zk, ConcurrentHashMap<String, Integer> vdpsTables){
		runnable = new VDPsCheckerRunnable(zk, vdpsTables);
		runnableThread = new Thread(runnable);
	}
	
	/**
	 * 
	 * @param zk Initialized ZooKeeper instance
	 * @param vdpsTables Key = tableName Value = lastVDP
	 * @param checkTimeout Timeout (in ms) between VDPs checks (must be greater than 300ms)
	 */
	public VDPsCheckerController(ZooKeeper zk, ConcurrentHashMap<String, Integer> vdpsTables, long checkTimeout){
		runnable = new VDPsCheckerRunnable(zk, vdpsTables, checkTimeout);
		runnableThread = new Thread(runnable);
	}
	
	public void startThread(){
		log.debug("{} - Starting VDPchecker Thread.",
				Thread.currentThread().getName());
		runnableThread.start();
	}
	
	public void stopThread(){
		log.debug("{} - Stopping VDPchecker Thread.",
				Thread.currentThread().getName());
		runnable.stopRunning();
		ZooKeeper.disconnect();
	}
	
	public boolean hasFinished(){
		return (runnableThread.isInterrupted() || !runnableThread.isAlive()) &&
				runnable.isStopped();
	}
	
	public void waitThreadTermination() throws InterruptedException{
		log.debug("{} - Waiting VDPchecker Thread Termination.",
				Thread.currentThread().getName());
		runnableThread.join();
	}
}
