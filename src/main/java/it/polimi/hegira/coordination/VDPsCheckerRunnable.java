/**
 * 
 */
package it.polimi.hegira.coordination;

import it.polimi.hegira.queue.events.MigratedVdpSource;
import it.polimi.hegira.zkWrapper.exception.OutOfSnapshotException;
import it.polimi.hegira.zkWrapper.statemachine.State;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Marco Scavuzzo
 *
 */
public class VDPsCheckerRunnable implements Runnable {
	private static final transient Logger log = LoggerFactory.getLogger(VDPsCheckerRunnable.class);
	private boolean stopThread = false;
	private ZooKeeper zk;
	private ConcurrentHashMap<String, Integer> vdpsTables;
	private long checkTimeout;
	private boolean isStopped = false;
	
	/**
	 * 
	 * @param zk Initialized ZooKeeper instance
	 * @param vdpsTables2 Key = tableName Value = lastVDP
	 */
	protected VDPsCheckerRunnable(ZooKeeper zk, ConcurrentHashMap<String, Integer> vdpsTables2){
		this.zk = zk;
		this.vdpsTables = vdpsTables2;
		checkTimeout = 3000;
	}
	
	/**
	 * 
	 * @param zk Initialized ZooKeeper instance
	 * @param vdpsTables Key = tableName Value = lastVDP
	 * @param checkTimeout Timeout (in ms) between VDPs checks (must be greater than 300ms)
	 */
	protected VDPsCheckerRunnable(ZooKeeper zk, ConcurrentHashMap<String, Integer> vdpsTables, long checkTimeout){
		this.zk = zk;
		this.vdpsTables = vdpsTables;
		if(checkTimeout>300){
			this.checkTimeout = checkTimeout;
		}else{
			checkTimeout = 3000;
		}
	}
	
	@Override
	public void run() {
		while(!stopThread && !vdpsTables.isEmpty()){
			checkLastVDPsStatus();
		}
		
		if(vdpsTables.isEmpty())
			log.info(Thread.currentThread().getName()+
						" - All VDPs, for all tables, have been migrated!");
		
		isStopped=true;
	}
	
	private void checkLastVDPsStatus(){
		Set<String> keys = vdpsTables.keySet();
		for(String key : keys){
			Integer vdpId = vdpsTables.get(key);
			try {
				State status = zk.getVDPstatus(key, vdpId.intValue());
				if(status != null && status.equals(State.MIGRATED)){
					//fire a changed state notification from the event source
					fireNotifications(key, vdpId);
					//remove vdp from local hashmap in order not to submit further notifications
					//for an already migrated vdp
					vdpsTables.remove(key);
				}
				
			} catch (OutOfSnapshotException e) {
				log.error(Thread.currentThread().getName()+
						" - You shouldn't be checking for VDP {}/{} here, because it is out of the snapshot!!",
						key, vdpId);
			}
		}
		try {
			Thread.sleep(checkTimeout);
		} catch (InterruptedException e) {
			log.error(Thread.currentThread().getName()+
						" - Error when trying to go to sleep.",e);
		}
	}
	
	private void fireNotifications(String tableName, Integer vdpId){
		log.info(Thread.currentThread().getName()+
						" - VDP {}/{} was MIGRATED. Notifying Listeners",
						tableName, vdpId);
		MigratedVdpSource.getInstance().notifyMigratedVdp(tableName, vdpId);
	}
	
	public void stopRunning(){
		stopThread = true;
		log.info(Thread.currentThread().getName()+
						" - Stopping thread");
	}
	
	public boolean isStopped(){
		return isStopped;
	}

}
