/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.queue.events;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class creates a <b>thread-safe</b> entry point to request slowdown of an ongoing data migration.
 * Classes that want/have to be notified of these requests must register themselves by means of 
 * the <code>addSlowDownListener</code> method.
 * @author Marco Scavuzzo
 */
public class SlowDownSource {
	private ConcurrentLinkedQueue<SlowDownListener> listeners;
	private transient Logger log = LoggerFactory.getLogger(SlowDownSource.class);

	private final static SlowDownSource instance =  new SlowDownSource();
	
	private SlowDownSource() {
		listeners = new ConcurrentLinkedQueue<SlowDownListener>();
	}
	
	/**
	 * @return The current instance of the {@link SlowDownSource} class.
	 */
	public static SlowDownSource getInstance() {
		return instance;
	}
	
	/**
	 * Registers a {@link SlowDownListener} that will be notified of pause requests.
	 * @param listener The listener class
	 */
	public void addSlowDownListener(SlowDownListener listener){
		this.listeners.add(listener);
	}
	
	/**
	 * Removes a given {@link SlowDownListener} from the list of listeners that will be notified.
	 * @param listener The listener class
	 */
	public void removeSlowDownListener(SlowDownListener listener){
		this.listeners.remove(listener);
	}

	/**
	 * Notifies all subscribed {@link SlowDownListener}s of a request to slow-down the current data migration.
	 * It creates a {@link SlowDownEvent} which is sent to all {@link SlowDownListener}s.
	 * @param timeout The overall duration (in milliseconds) of all pauses that will be issued in sequence to slow down the migration. 
	 * If <code>null</code> an infinite slow-down period is assumed.
	 * @param pausesDuration The duration of each pause (must be greater than 500ms). 
	 * It can be specified only if a timeout is provided.
	 */
	public void notifySlowDownRequest(Long timeout, long pausesDuration){
		SlowDownEvent event = new SlowDownEvent(this, timeout, pausesDuration);
		/*log.debug(Thread.currentThread().getName()+" - notifying slowdown:\n"
				+ "timeout: {},\n"
				+ "pausesDuration: {}\n"
				,timeout,pausesDuration);*/
		for(SlowDownListener l : listeners){
			l.onSlowDownRequest(event);
		}
		event = null;
	}
	
	/**
	 * Notifies all subscribed {@link SlowDownListener}s of a request to stop a previous slow-down policy.
	 * The data migration is speedup to normal speed.
	 */
	public void notifySlowDownTermination(){
		for(SlowDownListener l : listeners){
			l.onSlowDownTermination(null);
		}
	}
}
