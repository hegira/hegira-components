/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.queue.events;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * This class creates a <b>thread-safe</b> entry point to request pauses of an ongoing data migration.
 * Classes that want/have to be notified of these requests must register themselves by means of 
 * the <code>addPauseListener</code> method.
 * @author Marco Scavuzzo
 */
public class PauseSource {
	
	private ConcurrentLinkedQueue<PauseListener> listeners;
	
	private final static PauseSource instance = new PauseSource();
	
	private PauseSource() {
		listeners = new ConcurrentLinkedQueue<PauseListener>();
	}
	
	/**
	 * @return The current instance of the {@link PauseSource} class.
	 */
	public static PauseSource getInstance(){
		return instance;
	}

	/**
	 * Registers a {@link PauseListener} that will be notified of pause requests.
	 * @param listener The listener class
	 */
	public void addPauseListener(PauseListener listener){
		this.listeners.add(listener);
	}
	
	/**
	 * Removes a given {@link PauseListener} from the list of listeners that will be notified.
	 * @param listener The listener class
	 */
	public void removePauseListener(PauseListener listener){
		this.listeners.remove(listener);
	}
	
	/**
	 * Notifies all subscribed {@link PauseListener}s of a request to pause the current data migration.
	 * It creates a {@link PauseEvent} which is sent to all {@link PauseListener}s.
	 * @param timeout The duration of the pause. If <code>null</code> or less than 1ms an infinite pause is assumed. 
	 */
	public void notifyPauseRequest(Long timeout){
		PauseEvent event = new PauseEvent(this, timeout);
		for (PauseListener l : listeners) {
			l.onPauseRequest(event);
		}
		event = null;
	}
	
	/**
	 * Notifies all subscribed {@link PauseListener}s of a request to stop a previous pause.
	 * The data migration is resumed.
	 */
	public void notifyPauseTermination(){
		PauseEvent event = new PauseEvent(this, null);
		for (PauseListener l : listeners) {
			l.onPauseTermination(event);
		}
		event = null;
	}
}
