/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.queue.events;

import java.util.EventListener;

/**
 * The listener interface for receiving {@link SlowDownEvent}s.
 * The class that is interested in processing a slow-down events implements this interface, 
 * and the object created with that class is registered with the {@link SlowDownSource}, 
 * using the class' addSlowDownListener method. When the slow-down event occurs, that object's onSlowDownRequest method is invoked.
 * A previous slow-down request can also be revoked and the onSlowDownTermination method should be invoked.
 * 
 * @author Marco Scavuzzo
 */
public interface SlowDownListener extends EventListener {
	/**
	 * Invoked when a slow-down request should be issued. 
	 */
	public void onSlowDownRequest(SlowDownEvent evt);
	
	/**
	 * Invoked when a previous slow-down request should be revoked. 
	 */
	public void onSlowDownTermination(SlowDownEvent evt);
}
