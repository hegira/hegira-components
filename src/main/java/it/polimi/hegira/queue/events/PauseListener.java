/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.queue.events;

import java.util.EventListener;

/**
 * The listener interface for receiving {@link PauseEvent}s.
 * The class that is interested in processing a pause events implements this interface, 
 * and the object created with that class is registered with the {@link PauseSource}, 
 * using the class' addPauseListener method. When the pause event occurs, that object's onPauseRequest method is invoked.
 * A pause can also be stopped and the onPauseTermination method should be invoked.
 * 
 * @author Marco Scavuzzo
 */
public interface PauseListener extends EventListener {
	/**
	 * Invoked when a pause request should be issued. 
	 */
	public void onPauseRequest(PauseEvent evt);
	
	/**
	 * Invoked when a previous pause request should be revoked. 
	 */
	public void onPauseTermination(PauseEvent evt);
}
