package it.polimi.hegira.queue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author Marco Scavuzzo
 *
 */
public class VDPsQueue<E> {
	private ConcurrentHashMap<String, PriorityBlockingQueue<E>> map;
	private int queues_initialCapacity = 11;
	
	public VDPsQueue(){
		this.map = new ConcurrentHashMap<String, PriorityBlockingQueue<E>>();
	}
	
	/**
	 * Creates a PriorityBlockingQueue, associated to the given tableName, containing the elements in the specified collection. 
	 * If the specified collection is a SortedSet or a PriorityQueue, 
	 * this priority queue will be ordered according to the same ordering. 
	 * Otherwise, this priority queue will be ordered according to the natural ordering of its elements.
	 * @param tableName The name of the table
	 * @param c the collection whose elements are to be placed into this priority queue
	 */
	public VDPsQueue(String tableName, Collection<? extends E> c){
		this.map = new ConcurrentHashMap<String, PriorityBlockingQueue<E>>();
		map.put(tableName, new PriorityBlockingQueue<E>(c));
	}
	
	/**
	 * All PriorityBlockingQueues will be created with the specified initial capacity that orders its elements 
	 * according to their natural ordering.
	 * @param queues_initialCapacity the initial capacity for this priority queue
	 */
	public VDPsQueue(int queues_initialCapacity){
		this.map = new ConcurrentHashMap<String, PriorityBlockingQueue<E>>();
		this.queues_initialCapacity = queues_initialCapacity;
	}
	
	/**
	 * Inserts the specified element into the proper priority queue.
	 * @param tableName The name of the table containing the given VDPid
	 * @param e The VDPid
	 * @return true if this collection changed as a result of the call
	 */
	public boolean add(String tableName, E e) {
		if(e==null || tableName==null) return false;
		
		boolean added;
		if(map.containsKey(tableName)){
			added = map.get(tableName).add(e);
		}else{
			PriorityBlockingQueue<E> queue = new PriorityBlockingQueue<E>(queues_initialCapacity);
			added = queue.add(e);
			map.put(tableName, queue);
		}
		return added;
	}
	
	/**
	 * Atomically removes all of the elements from the proper queue. The queue will be empty after this call returns.
	 * @param tableName
	 */
	public void clear(String tableName){
		if(tableName==null) return;
		
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue!=null)
			queue.clear();
	}
	
	/**
	 * Atomically removes all of the elements from all queues.
	 */
	public void clearAll(){
		Set<String> map_keys = map.keySet();
		for(String key : map_keys){
			clear(key);
		}
	}
	
	/**
	 * Returns true if the proper queue contains the specified element. 
	 * More formally, returns true if and only if the proper queue contains at least one element e such that o.equals(e).
	 * @param tableName The table name
	 * @param o The element to look for
	 * @return true if this queue contains the specified element
	 */
	public boolean contains(String tableName, E o){
		if(tableName==null || o == null) return false;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return false;
		}else{
			return queue.contains(o);
		}
	}
	
	/**
	 * Inserts the specified element into the proper priority queue. 
	 * As the queue is unbounded, this method will return false only if the map does not contain the queue.
	 * @param tableName
	 * @param e the element to add
	 * @return As the queue is unbounded, this method will return false only if the map does not contain the queue.
	 */
	public boolean offer(String tableName, E e){
		if(tableName==null || e == null) return false;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return false;
		}else{
			return queue.offer(e);
		}
	}
	
	/**
	 * Inserts the specified element into the proper priority queue. 
	 * As the queue is unbounded, this method will return false only if the map does not contain the queue.
	 * @param tableName
	 * @param e the element to add
	 * @param timeout This parameter is ignored as the method never blocks
	 * @param unit This parameter is ignored as the method never blocks
	 * @return As the queue is unbounded, this method will return false only if the map does not contain the queue.
	 */
	public boolean offer(String tableName, E e,  long timeout, TimeUnit unit){
		if(tableName==null || e == null) return false;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return false;
		}else{
			return queue.offer(e, timeout, unit);
		}
	}
	
	/**
	 * Retrieves, but does not remove, the head of the proper queue, or returns null if this queue is empty.
	 * @param tableName
	 * @return the head of the proper queue, or null if this is empty
	 */
	public E peek(String tableName){
		if(tableName==null) return null;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return null;
		}else{
			return queue.peek();
		}
	}
	
	/**
	 * Retrieves and removes the head of the proper queue, or returns null if this queue is empty.
	 * @param tableName
	 * @return
	 */
	public E poll(String tableName){
		if(tableName==null) return null;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return null;
		}else{
			return queue.poll();
		}
	}
	
	/**
	 * Retrieves and removes the head of the proper queue, waiting up to the specified wait time 
	 * if necessary for an element to become available.
	 * @param tableName 
	 * @param timeout how long to wait before giving up, in units of unit
	 * @param unit a TimeUnit determining how to interpret the timeout parameter
	 * @return the head of the proper queue, or null if the specified waiting time elapses before an element is available
	 * @throws InterruptedException
	 */
	public E poll(String tableName, long timeout,
		     TimeUnit unit) throws InterruptedException {
		if(tableName==null) return null;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return null;
		}else{
			return queue.poll(timeout, unit);
		}
	}
	
	/**
	 * Inserts the specified element into the proper priority queue. 
	 * As the queue is unbounded, this method will never block.
	 * @param tableName
	 * @param e the element to add
	 */
	public void put(String tableName, E e){
		if(e==null || tableName==null) return;
		
		if(map.containsKey(tableName)){
			map.get(tableName).put(e);
		}else{
			PriorityBlockingQueue<E> queue = new PriorityBlockingQueue<E>(queues_initialCapacity);
			queue.put(e);
			map.put(tableName, queue);
		}
	}
	
	/**
	 * Removes a single instance of the specified element from the proper queue, if it is present. 
	 * More formally, removes an element e such that o.equals(e), if this queue contains one or more such elements. 
	 * Returns true if and only if this queue contained the specified element (or equivalently, if this queue changed as a result of the call).
	 * @param tableName
	 * @param o element to be removed from this queue, if present
	 * @return true if this queue changed as a result of the call
	 */
	public boolean remove(String tableName, E o){
		if(tableName==null || o == null) return false;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return false;
		}else{
			return queue.remove(o);
		}
	}
	
	/**
	 * Returns the number of elements in the proper collection. 
	 * If this collection contains more than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
	 * @param tableName
	 * @return the number of elements in the proper collection
	 */
	public int size(String tableName){
		if(tableName==null) return 0;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return 0;
		}else{
			return queue.size();
		}
	}
	
	/**
	 * Retrieves and removes the head of the proper queue, waiting if necessary until an element becomes available.
	 * @param tableName
	 * @return the head of this queue
	 * @throws InterruptedException if interrupted while waiting
	 */
	public E take(String tableName)
		       throws InterruptedException{
		if(tableName==null) return null;
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return null;
		}else{
			return queue.take();
		}
	}
	
	/**
	 * Returns an array containing all of the elements in the proper queue. The returned array elements are in no particular order.
	 * The returned array will be "safe" in that no references to it are maintained by this queue. 
	 * (In other words, this method must allocate a new array). The caller is thus free to modify the returned array.
	 * This method acts as bridge between array-based and collection-based APIs.
	 * @param tableName
	 * @return an array containing all of the elements in the proper queue
	 */
	public Object[] toArray(String tableName){
		if(tableName==null) return new Object[0];
		PriorityBlockingQueue<E> queue = map.get(tableName);
		if(queue==null){ 
			return new Object[0];
		}else{
			return queue.toArray();
		}
	}
	
	/**
	 * Adds all of the elements in the specified collection to the proper queue. 
	 * Attempts to addAll of a queue to itself result in IllegalArgumentException. 
	 * Further, the behavior of this operation is undefined if the specified collection is modified while the operation is in progress.
	 * This implementation iterates over the specified collection, and adds each element returned by the iterator to this queue, in turn. 
	 * A runtime exception encountered while trying to add an element (including, in particular, a null element) 
	 * may result in only some of the elements having been successfully added when the associated exception is thrown.
	 * @param tableName
	 * @param c collection containing elements to be added to the proper queue
	 * @return true if the proper queue changed as a result of the call
	 */
	public boolean addAll(String tableName, Collection<? extends E> c){
		if(c==null || tableName==null) return false;
		
		boolean added;
		if(map.containsKey(tableName)){
			added = map.get(tableName).addAll(c);
		}else{
			PriorityBlockingQueue<E> queue = new PriorityBlockingQueue<E>(queues_initialCapacity);
			added = queue.addAll(c);
			map.put(tableName, queue);
		}
		return added;
	}
	
	/**
	 * Returns true if the proper collection contains no elements. 
	 * @param tableName
	 * @return true if the proper collection contains no elements
	 */
	public boolean isEmpty(String tableName){
		return size(tableName)==0;
	}
	
	@Override
	public String toString() {
		Set<String> keys = map.keySet();
		StringBuilder sbAll = new StringBuilder();
		for(String key : keys){
			sbAll.append("Table: "+key+"\n");
			ArrayList<E> queue_copy = new ArrayList<E>(map.get(key));
			StringBuilder sb = new StringBuilder();
			sb.append("\t[");
			for(E e : queue_copy){
				sb.append(e+" ");
			}
			sb.append("]");
			sbAll.append(sb.toString()+"\n");
		}
		return sbAll.toString();
	}
}
