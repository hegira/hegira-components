/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.queue;

import it.polimi.hegira.exceptions.QueueException;
import it.polimi.hegira.queue.events.PauseEvent;
import it.polimi.hegira.queue.events.PauseListener;
import it.polimi.hegira.queue.events.SlowDownEvent;
import it.polimi.hegira.queue.events.SlowDownListener;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.DefaultErrors;
import it.polimi.hegira.zkWrapper.ZKcomponentStatus;
import it.polimi.hegira.zkWrapper.statemachine.MigrationComponentStatus;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP.Queue.PurgeOk;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * Task queue used by:
 * 	1. the SRC to write Metamodel entities;
 *  2. the TWC to read Metamodel entities;
 *  
 *  It implements the {@link PauseListener} and the {@link SlowDownListener} interfaces 
 *  to receive {@link PauseEvent}s and {@link SlowDownEvent}s and act accordingly. 
 *  
 * @author Marco Scavuzzo
 */
public class TaskQueue implements PauseListener, SlowDownListener {
	private static Logger log = LoggerFactory.getLogger(TaskQueue.class);

	private ConnectionFactory factory;
	private Connection connection;
	private Channel channel;
	private QueueingConsumer consumer;
	protected static final String TASK_QUEUE_NAME = "task_queue";
	private String mode;
	
	protected int THREADS_NO = 10;
	private int MAX_THREADS_NO = 60;
	
	private SlowDownEvent slowdownReq;
	private Boolean slowed = false;
	private PauseEvent pauseReq;
	private Boolean paused = false;
	
	private ZKcomponentStatus zkStatuses;

	/**
	 * Creates a task queue between the SRC and the TWC.
	 * @param mode The component calling it, i.e. SRC or TWC
	 * @param threads The number of threads that will consume from the queue (only for the TWC).
	 * @param queueAddress The address where the RabbitMQ broker is deployed. Default: localhost
	 * @throws QueueException If a connection cannot be established.
	 */
	public TaskQueue(String mode, int threads, String queueAddress) throws QueueException{
		if(mode==null) return;
		
		this.mode=mode;
		factory = new ConnectionFactory();
		if(queueAddress==null || queueAddress.isEmpty()){
			factory.setHost("localhost");
		}else{
			factory.setHost(queueAddress);
		}
		
		try {
			connection = factory.newConnection();
			channel = connection.createChannel();
			/**
			 * Declaring a durable queue
			 * queueDeclare(java.lang.String queue, boolean durable, 
			 * boolean exclusive, boolean autoDelete, 
			 * java.util.Map<java.lang.String,java.lang.Object> arguments) 
			 */
			channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
			
			 //queue settings differentiation
			switch(mode){
				case Constants.PRODUCER:
					consumer=null;
					break;
				case Constants.CONSUMER:
					if(threads>10 && threads <= 60){
						this.THREADS_NO = threads;
					}else if(threads>60){
						this.THREADS_NO = MAX_THREADS_NO;
						log.info(DefaultErrors.getThreadsInformation(MAX_THREADS_NO));
					}
					if(factory.getHost().equals("localhost"))
						channel.basicQos(1);
					else
						channel.basicQos(20);
					consumer = new QueueingConsumer(channel);
					/**
					 * basicConsume(java.lang.String queue, boolean autoAck, Consumer callback)
					 * Starts a non-nolocal, non-exclusive consumer, 
					 * with a server-generated consumerTag.
					 */
					channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
					break;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new QueueException(e.getMessage());
		}
	}
	
	public void changeBaseQosPerConsumer(int newQos) throws IOException{
		if(mode!=null && mode.equals(Constants.PRODUCER)){
			throw new IllegalStateException("Cannot be called by a Producer!");
		}
		channel.close();
		connection.close();
		
		connection = factory.newConnection();
		channel = connection.createChannel();
		
		channel.basicQos(newQos);
		//consumer.getChannel().basicQos(newQos);
		consumer = new QueueingConsumer(channel);
		channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
	}
	
	/**
	 * This method is called by SRTs or TWTs before production/consumption.
	 * The method uses a monitor on the {@link PauseEvent} <code>pauseReq</code>.
	 * Causes the current thread (SRT or TWT) to issue small pauses until another thread (main) invokes the 
	 * {@link TaskQueue.onSlowDownTermination(SlowDownEvent evt)} method or until the timeout, if not null, is expired.
	 */
	private void applySlowDownPolicy() {
		if(slowed){	
			try{
				if(this.slowdownReq!=null && this.slowdownReq.getPausesNo()==-1){
					//log.info(Thread.currentThread().getName()+
					//	" - slowing down indefinitely, until speedup event is received!");
					//just sleep
					Thread.sleep(this.slowdownReq.getPausesDuration());
				}else if(this.slowdownReq!=null && this.slowdownReq.getPausesNo()>0){
					/*log.info(Thread.currentThread().getName()+
							" - slowing down "+this.slowdownReq.getPausesNo()+" times, for "+
							+this.slowdownReq.getPausesDuration()+" ms "
							+ "or until speedup event is received!");*/
					this.slowdownReq.decrementPausesNo();
					Thread.sleep(this.slowdownReq.getPausesDuration());
				}else{
					log.info(Thread.currentThread().getName()+
							" - Ending slow down period");
					synchronized(slowed){slowed=false;}
				}
			}catch(InterruptedException e){
				log.error(Thread.currentThread().getName()+
						" - Error in slowing down ");
				synchronized(slowed){
					slowed=false;
					//persisting change
					if(zkStatuses!=null){
						MigrationComponentStatus status = retrieveComponentStatus();
						status.speedupMigration();
						zkStatuses.setMigrationComponentStatus(status);
					}
				}
			}
		}
	}
	
	/**
	 * This method is called by SRTs or TWTs before production/consumption.
	 * The method uses a monitor on the {@link PauseEvent} <code>pauseReq</code>.
	 * Causes the current thread (SRT or TWT) to wait until another thread (main) invokes the 
	 * {@link TaskQueue.onPauseTermination(PauseEvent evt)} method or until the timeout, if not null, is expired.
	 */
	private void applyPausePolicy() {
		if(paused){
			long timeBeforeWait = System.currentTimeMillis();
			boolean exception=false;
			try {
				if(pauseReq!=null && pauseReq.getTimeout()==null){
					log.debug(Thread.currentThread().getName()+
							" - !! going to sleep for indefinitely");
					synchronized (pauseReq) {
						pauseReq.wait();	
					}
				}else if(pauseReq!=null &&  pauseReq.getTimeout().longValue()>0){
					log.debug(Thread.currentThread().getName()+
							" - !! going to sleep for "+pauseReq.getTimeout().longValue()+
							"ms!");
					synchronized (pauseReq) {
						pauseReq.wait(pauseReq.getTimeout().longValue());
					}
				} else {
					log.debug(Thread.currentThread().getName()+
							" - !! going to sleep for indefinitely");
					synchronized (pauseReq) {
						pauseReq.wait();	
					}
				}
			} catch (InterruptedException e) {exception=true;}
			
			long timeAfterWait = System.currentTimeMillis();
			long waitingTime = timeAfterWait - timeBeforeWait;
			if(!exception)
				log.debug(Thread.currentThread().getName()+
						" - Awakeining after sleeping for "+waitingTime+
						"ms!");
			else {
				log.debug(Thread.currentThread().getName()+
						" - Awakeining after Exception. Waited for "+waitingTime+
						"ms!");
				//persisting change
				if(zkStatuses!=null){
					MigrationComponentStatus status = retrieveComponentStatus();
					status.unpauseMigration();
					zkStatuses.setMigrationComponentStatus(status);
				}
			}
			synchronized(paused){
				paused=false;
			}
		}
	}
	
	/**
	 * Publishes a message in the task queue
	 * @param message The message to be published
	 * @throws QueueException if an error is encountered
	 */
	public void publish(byte[] message) throws QueueException{
		try {
			applyPausePolicy();
			/**
			 * void basicPublish(java.lang.String exchange,
	         *       java.lang.String routingKey,
	         *       AMQP.BasicProperties props,
	         *       byte[] body)
			 */
			channel.basicPublish("", TASK_QUEUE_NAME, null, message);
			applySlowDownPolicy();
		} catch (IOException e) {
			throw new QueueException(e.getMessage(), e.getCause());
		}
	}
	
	/**
	 * Simple method that retrieves the next message from the task queue.
	 * Wait for the next message delivery and return it.
	 * @return The message
	 * @throws ShutdownSignalException if an interrupt is received while waiting
	 * @throws ConsumerCancelledException if the connection is shut down while waiting
	 * @throws InterruptedException if this consumer is cancelled while waiting
	 */
	public Delivery consume() throws ShutdownSignalException, ConsumerCancelledException, InterruptedException{
		applyPausePolicy();
		applySlowDownPolicy();
		return getConsumer().nextDelivery();
		
	}
	
	/**
	 * Wait for the next message delivery and return it.
	 * @param timeout
	 * @return the next message or null if timed out
	 * @throws ShutdownSignalException if an interrupt is received while waiting
	 * @throws ConsumerCancelledException if the connection is shut down while waiting
	 * @throws InterruptedException if this consumer is cancelled while waiting
	 */
	public Delivery consume(long timeout) throws ShutdownSignalException, ConsumerCancelledException, InterruptedException{
		applySlowDownPolicy();
		applyPausePolicy();
		return getConsumer().nextDelivery(timeout);
	}
	
	/**
	 * Acknowledge a message given its delivery tag.
	 * @param deliveryTag
	 */
	public void sendAck(long deliveryTag) throws QueueException{
		try {
			channel.basicAck(deliveryTag, false);
		} catch (IOException e) {
			throw new QueueException(e.getMessage(),e.getCause());
		}
	}
	
	/**
	 * Acknowledge a message given its delivery (returned from the QueuingConsumer object).
	 * @param delivery
	 * @throws QueueException
	 */
	public void sendAck(Delivery delivery) throws QueueException{
		try {
			channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
		} catch (IOException e) {
			throw new QueueException(e.getMessage(),e.getCause());
		}
	}
	
	public void sendAckLast(long deliveryTag) throws QueueException{
		try {
			channel.basicAck(deliveryTag, true);
		} catch (IOException e) {
			throw new QueueException(e.getMessage(),e.getCause());
		}
	}
	
	public void sendAcks(List<Long> deliveryTags) throws QueueException{
		//Manually set.
		//true should require less network round trips for acks from/to rabbitmq
		boolean efficient=true;
		if(efficient){
			if(deliveryTags==null)
				return;
			if(deliveryTags.isEmpty())
				return;
			if(deliveryTags.size()==1){
				sendAck(deliveryTags.get(0));
				return;
			}
			
			Collections.sort(deliveryTags);
			long prev=-1;
			boolean ackRange=true;
			for(int i=0;i<deliveryTags.size();i++){
				if(prev==-1){
					prev=deliveryTags.get(i);
					continue;
				}
				
				long curr=deliveryTags.get(i);
				if(curr!=prev+1 && ackRange){
					ackRange=false;
					sendAckLast(prev);
				}
				
				if(!ackRange){
					sendAck(curr);
				}
				prev=curr;
			}
			
			if(ackRange){
				long max=deliveryTags.get(deliveryTags.size()-1);
				sendAckLast(max);
			}
		}else{
			int expAcks = deliveryTags.size(), effAcks=0;
			for(Long dt : deliveryTags) {
				//log.debug("Acking delivery tag: "+dt);
				sendAck(dt.longValue());
				effAcks++;
			}
			if(expAcks!=effAcks){
				if(expAcks<Integer.MAX_VALUE)
					log.error("{} - Acknowledged {} messages out of {}",
							Thread.currentThread().getName(),
							effAcks, expAcks);
				else
					log.error("{} - Too many deliveries stored",
							Thread.currentThread().getName());
			}
		}
	}
	
	/**
	 * In case a message cannot be processed properly a negative acknowledgment must be sent.
	 * @param delivery The message that was not processed.
	 * @throws QueueException
	 */
	public void sendNack(Delivery delivery) throws QueueException{
		try {
			/**
			 * void basicNack(long deliveryTag,
             *					boolean multiple,
             *					boolean requeue)
			 */
			channel.basicNack(delivery.getEnvelope().getDeliveryTag(),
					false, true);
			/**
			 * void basicReject(long deliveryTag,
             *					boolean requeue)
			 */
			channel.basicReject(delivery.getEnvelope().getDeliveryTag(), true);
		} catch (IOException e) {
			throw new QueueException(e.getMessage(),e.getCause());
		}
	}
	
	public void sendNack(long deliveryTag) throws QueueException {
		try {
			channel.basicNack(deliveryTag, false, true);
			channel.basicReject(deliveryTag, true);
		} catch (IOException e){
			throw new QueueException(e.getMessage(),e.getCause());
		}
	}
	
	public void sendNacks(List<Long> deliveryTags) throws QueueException {
		for(Long dt : deliveryTags){
			sendNack(dt);
		}
	}
	
	/**
	 * Returns an approximation of the messages present in the queue.
	 * NB. Sometimes the count may be 0.
	 * @param queue_name The name of the queue to query.
	 * @return	The number of messages in the queue.
	 */
	public int getMessageCount(String queue_name){
		try {
			return channel.queueDeclarePassive(queue_name).getMessageCount();
		} catch (IOException e) {
			log.debug("Error reading message count from "+queue_name, e);
			return 0;
		}
	}
	
	/**
	 * Gets the task queue consumer.
	 * @return The Queuing consumer.
	 */
	public QueueingConsumer getConsumer(){
		return consumer;
	}
	
	public static String getDefaultTaskQueueName(){
		return TASK_QUEUE_NAME;
	}
	
	private int queueElements = 0;
	private long previousQueueCheckTime=0;
	
	/**
	 * When called, determines if the SRC produces too fast for the TWC which consumes.
	 * If it is the case, it slows down the production.
	 * Should be used only for non-partitioned migration.
	 */
	public void slowDownProduction(){
		
		int messageCount = getMessageCount(TASK_QUEUE_NAME);
		
		//Producer is faster then consumers
		if(messageCount-queueElements>0 && messageCount>50000){
			long consumingRate = (messageCount-queueElements)/
					(System.currentTimeMillis() - previousQueueCheckTime);
		
			if(consumingRate<=0) consumingRate=1;
			/*
			 * How much time should I wait to lower the queue to 50'000entities?
			 * t = (messageCount(ent) - 50000(ent))/consumingRate(ms)
			 * Anyway, wait no more than 40s
			 */
			long t = (messageCount - 50000)/consumingRate;
			if(t>40000) t=40000;
			if(t<0) t=0;
			
			log.debug("Consuming rate: "+consumingRate+"ent/ms. \tSlowing down ... wait "+t+" ms");
			
			try {
				Thread.sleep(t);
			} catch (InterruptedException e) {
				log.error("Cannot puase", e);
			}
		
			queueElements = getMessageCount(TASK_QUEUE_NAME);
			previousQueueCheckTime = System.currentTimeMillis();
			
		}
	}
	
	/**
	 * Purges the task queue.
	 * @return <b>true</b> if purged; <b>false</b> otherwise.
	 * @throws IOException
	 * @throws ShutdownSignalException
	 * @throws ConsumerCancelledException
	 * @throws InterruptedException
	 * @throws QueueException
	 */
	public boolean purgeQueue() throws IOException, ShutdownSignalException, ConsumerCancelledException, InterruptedException, QueueException{
		PurgeOk queuePurge = channel.queuePurge(TASK_QUEUE_NAME);
		if(queuePurge!=null){
			boolean wasNull=false;
			if(consumer==null){
				wasNull=true;
				consumer = new QueueingConsumer(channel);
				channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
			}
			Delivery delivery = consumer.nextDelivery(50);
			if(delivery!=null){
				sendAck(delivery);
				//log.debug(Thread.currentThread().getName()+
				//		" consumed orphan");
			}
			//log.debug(Thread.currentThread().getName()+
			//		" message count: "+queuePurge.getMessageCount());
			if(wasNull)
				consumer=null;
		}
		return (queuePurge != null) ?  true :  false;
	}

	@Override
	public void onSlowDownRequest(SlowDownEvent evt) {
		if(evt!=null){
			synchronized(slowed){
				log.debug(Thread.currentThread().getName()+
						" - Asking to slow down\n");
				this.slowdownReq = evt;
				slowed=true;
				log.debug(Thread.currentThread().getName()+
						" - Setted slowdown event: \n"+evt.toString());
				//persisting change
				if(zkStatuses!=null){
					MigrationComponentStatus status = retrieveComponentStatus();
					status.slowdownMigration();
					zkStatuses.setMigrationComponentStatus(status);
				}
			}
		}
	}
	
	@Override
	public void onSlowDownTermination(SlowDownEvent evt) {
		synchronized(slowed){
			log.debug(Thread.currentThread().getName()+
					" - Removing slow down request");
			this.slowdownReq = null;
			slowed=false;
			//persisting change
			if(zkStatuses!=null){
				MigrationComponentStatus status = retrieveComponentStatus();
				status.speedupMigration();
				zkStatuses.setMigrationComponentStatus(status);
			}
		}
	}

	@Override
	public void onPauseRequest(PauseEvent evt) {
		if(evt!=null){
			synchronized(paused){
				paused=true;
				pauseReq=evt;
				//persisting change
				if(zkStatuses!=null){
					MigrationComponentStatus status = retrieveComponentStatus();
					status.pauseMigration();;
					zkStatuses.setMigrationComponentStatus(status);
				}
			}
		}else{
			log.error(Thread.currentThread().getName()+" - The PauseEvent must not be null");
		}
			
	}

	@Override
	public void onPauseTermination(PauseEvent evt) {
		synchronized(pauseReq){
			Log.debug(Thread.currentThread().getName()+
					" - Removing pause request");
			synchronized(paused){
				paused=false;
				//persisting change
				if(zkStatuses!=null){
					MigrationComponentStatus status = retrieveComponentStatus();
					status.unpauseMigration();
					zkStatuses.setMigrationComponentStatus(status);
				}
			}
			pauseReq.notify();
		}
	}

	public void setZkComponentsStatuses(ZKcomponentStatus zkStatuses) {
		this.zkStatuses = zkStatuses;
	}
	
	private MigrationComponentStatus retrieveComponentStatus(){
		MigrationComponentStatus prev = zkStatuses.getMigrationComponentStatus(getComponentName(), getMigrationId());
		if(prev==null){
			prev = new MigrationComponentStatus(getMigrationId(), getComponentName());
		}
		return prev;
	}
	
	private String getComponentName(){
		return consumer==null ? "SRC" : "TWC";
	}
	
	//TODO: implement migration id support!
	private int getMigrationId(){
		return 1;
	}
	
	
}
