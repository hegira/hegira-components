/**
 * 
 */
package it.polimi.hegira.utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.polimi.hegira.models.Column;
import it.polimi.hegira.models.Metamodel;

/**
 * @author Marco Scavuzzo
 *
 */
public class TestsUtils {

	public static Metamodel getMetamodelTestingEntity() throws IOException{
		ByteBuffer bb = ByteBuffer.wrap(DefaultSerializer.serialize(new Integer(117)));
		Column column = new Column("testC", bb, Integer.class.getSimpleName(), false);
		ArrayList<Column> list = new ArrayList<Column>(1);
		list.add(column);
		HashMap<String, List<Column>> map = new HashMap<String,List<Column>>();
		map.put("cf1", list);
		ArrayList<String> cfs = new ArrayList<String>(1);
		cfs.add("cf1");
		
		Metamodel mm = new Metamodel("@table1#PG1", "r1", map);
		mm.setColumnFamilies(cfs);
		
		return mm;
	}
}
