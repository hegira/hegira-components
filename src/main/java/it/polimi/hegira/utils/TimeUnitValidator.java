package it.polimi.hegira.utils;

import java.util.concurrent.TimeUnit;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class TimeUnitValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		if(value.equals(TimeUnit.SECONDS.name()) ||
				value.equals(TimeUnit.MINUTES.name()) ||
				value.equals(TimeUnit.HOURS.name())) 
			return;
		else {
			throw new ParameterException("Parameter "+name+" supported values are: "+
					TimeUnit.SECONDS.name()+", "+
					TimeUnit.MINUTES.name()+", "+
					TimeUnit.HOURS.name()+".");
		}

	}

}
