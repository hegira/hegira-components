/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationManagerCassandra {
	private static Logger log=LoggerFactory.getLogger(ConfigurationManagerCassandra.class);
	private static String defaultPath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"components"+File.separator;
	
	private static String getPropertiesFromFile(String fileName,String propertyKey){
		log.debug("trying to read configuration info!!!");
		Properties properties=new Properties();
		InputStream inputStream = null;
		try{
			inputStream = new FileInputStream(createFileIfNotExists(defaultPath, fileName));
			properties.load(inputStream);
			return properties.getProperty(propertyKey);
		}catch(FileNotFoundException | NullPointerException e){
			log.error(fileName+" file has to exist");
		}catch(IOException e){
			log.error("Unable to read file "+fileName);
		}finally{
			try {
				if(inputStream!=null)
					inputStream.close();
			} catch (IOException e) {}
			properties=null;
		}
		
		return null;
	}
	
	public static String getConfigurationProperties(String propertyKey){
		return getPropertiesFromFile(Constants.CASSANDRA_CONFIGURATION_FILE, propertyKey);
	}
	
	private static File createFileIfNotExists(String path, String fileName) 
			throws NullPointerException, IOException{
		if(fileName == null)
			throw new NullPointerException("fileName cannot be null");
		if(path==null)
			path=defaultPath;
		
		File file = new File(path+fileName);
		if (!file.getParentFile().exists())
		    file.getParentFile().mkdirs();
		if (!file.exists())
		    file.createNewFile();
		return file;
	}
}
