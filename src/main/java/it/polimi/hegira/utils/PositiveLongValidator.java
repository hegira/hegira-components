package it.polimi.hegira.utils;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class PositiveLongValidator implements IParameterValidator {

	@Override
	public void validate(String name, String value) throws ParameterException {
		try{
			long longValue = Long.parseLong(value);
			if(longValue<0)
				throw new ParameterException("Parameter "+name+" should be positive!");
		}catch(NumberFormatException e){
			throw new ParameterException("Parameter "+name+" should be positive number!");
		}
	}

}
