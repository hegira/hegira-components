/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to retrieve data from properties files.
 * @author Marco Scavuzzo
 *
 */
public class PropertiesManager {
	private transient static Logger log = LoggerFactory.getLogger(PropertiesManager.class);
	private static String defaultPath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"components"+File.separator;
	private static String FILTER = "filter.properties";
	
	/**
	 * Gets the value of a given property stored inside the credentials file.
	 * @param property	The name of the property.
	 * @return	The value for the given property name.
	 */
	public static String getCredentials(String property){
		return getProperty(Constants.CREDENTIALS_PATH, property);
	}
	
	/**
	 * Gets the value of a given property stored inside the given file.
	 * @param fileName the file name in the default path (user.home)
	 * @param property	The name of the property to retrieve.
	 * @return	The value associated to the given property name.
	 */
	public static String getProperty(String fileName, String property){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(createFileIfNotExists(defaultPath, fileName));	
			props.load(isr);
			return props.getProperty(property);
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	public static List<String> getIncludedTables(){
		String propertyName = "included";
		String propertyValue = PropertiesManager.getProperty(FILTER, propertyName);
		return getListFromProperty(propertyValue);
	}
	
	public static List<String> getExcludedTables(){
		String propertyName = "excluded";
		String propertyValue = PropertiesManager.getProperty(FILTER, propertyName);
		return getListFromProperty(propertyValue);
	}
	
	/**
	 * 
	 * @param property The property value
	 * @return
	 */
	private static List<String> getListFromProperty(String property){
		ArrayList<String> words = new ArrayList<String>();
		if(property==null){
			//do nothing
		}else{
			if(property.contains(",")){
				int end = property.length(), curr=0;
				while(curr<end){
					int new_curr = property.indexOf(",", curr);
					if(new_curr==-1) new_curr = end;
					//log.debug("Checking range [{},{}]",curr,new_curr);
					words.add(property.substring(curr, new_curr));
					curr=new_curr+1;
				}
			}else{
				words.add(property);
			}
		}
		
		return words;
	}
	/**
	 * Gets several properties from the same file at the same time
	 * @param fileName the file name in the default path (user.home)
	 * @param properties The names of the properties to retrieve.
	 * @return The values associated to the given properties names.
	 */
	public static Map<String,String> getProperties(String fileName, String... properties){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(createFileIfNotExists(defaultPath, fileName));	
			props.load(isr);
			HashMap<String,String> map = new HashMap<String,String>();
			for(String pName : properties){
				String prop = props.getProperty(pName);
				if(prop!=null)
					map.put(pName, prop);
			}
			return map;
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	private static File createFileIfNotExists(String path, String fileName) 
			throws NullPointerException, IOException{
		if(fileName == null)
			throw new NullPointerException("fileName cannot be null");
		if(path==null)
			path=defaultPath;
		
		File file = new File(path+fileName);
		if (!file.getParentFile().exists())
		    file.getParentFile().mkdirs();
		if (!file.exists())
		    file.createNewFile();
		return file;
	}
	
	/**
	 * Gets a property stored in a property file within the same JAR package of this code.
	 * @param file The file name.
	 * @param property The property name to retrieve.
	 * @return The retrieved value associated with the given property name.
	 */
	public static String getPropertyFromJar(String file, String property){
		Properties props = new Properties();
		try {
			InputStream isr = PropertiesManager.class.getResourceAsStream("/"+file);
			
			if (isr == null){
				throw new FileNotFoundException(file + " must exist.");
			}
			props.load(isr);
			return props.getProperty(property);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			return null;
		} finally {
			props=null;
		}
		return null;
	}
	
	/**
	 * Gets the ZooKeeper connect string from the credentials file.
	 * @return	ZooKeeper connect string (i.e., ip_address:port).
	 */
	public static String getZooKeeperConnectString(){
		return getCredentials(Constants.ZK_CONNECTSTRING);
	}
	
	public static String getHBaseQuorum(){
		return getProperty(Constants.HBASE_CONFIGURATION_FILE,
				"hbase.zookeeper.quorum");
	}
	
	public static String getHBaseProperty(String propertyName){
		return getProperty(Constants.HBASE_CONFIGURATION_FILE,
				propertyName);
	}
	
	public static org.apache.hadoop.hbase.client.Consistency getHBaseConsistencyType() {
		boolean isTimeline = Boolean.parseBoolean(getProperty(Constants.HBASE_CONFIGURATION_FILE,
				"consistency.timeline"));
		if (isTimeline)
			return org.apache.hadoop.hbase.client.Consistency.TIMELINE;
		else
			return org.apache.hadoop.hbase.client.Consistency.STRONG;
	}
	
}
