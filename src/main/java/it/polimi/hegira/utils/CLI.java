/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.utils;

import java.util.concurrent.TimeUnit;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 * Defines the parameters needed to launch the program.
 * @author Marco Scavuzzo
 */
public class CLI {
	
	@Parameter(names = { "--type", "-t" }, description = "launch a SRC or a TWC",
			required = true, validateWith = ComponentValidator.class)
	public String componentType;
	
	@Parameter(names = { "--queue","-q" }, description = "RabbitMQ queue address.")
	public String queueAddress = "localhost";
	
	@Parameters(commandDescription = "Executes the component in test mode, allowing the user to provide additional parameters")
	public class TestsCommand {
		public static final String commandName = "tests";
		
		@Parameter(names = { "--sDelay","-d" }, 
				description = "The time from now to delay execution of the task that will stop the component's threads (SRTs or TWTs).",
				validateWith=PositiveLongValidator.class)
		public long stopDelay = -1;
		
		@Parameter(names = {"--unit", "-u"},
				description = "The time unit of the delay parameter.",
				validateWith = TimeUnitValidator.class)
		public String stopDelayUnit = TimeUnit.SECONDS.name();
		
		@Parameter(names = { "--eName","-e" }, 
				description = "A name should be given to the statistics.")
		public String expName = "exp"+System.currentTimeMillis();
		
		@Parameter(names = { "--iDelay","-i" }, 
				description = "The initial time to delay sampling execution.",
				validateWith=PositiveLongValidator.class)
		public long initialDelay = 1;
		
		@Parameter(names = { "--samPer","-p" }, 
				description = "The period between successive samples.",
				validateWith=PositiveLongValidator.class)
		public long samplingPeriod = -1;
		
		@Parameter(names = {"--unitSam", "-us"},
				description = "The time unit of both the init-delay and sample-period parameters.",
				validateWith = TimeUnitValidator.class)
		public String sampleUnit = TimeUnit.MINUTES.name();
		
		public TimeUnit parseTimeUnit(String unit){
			if(unit.equals(TimeUnit.SECONDS.name())){
				return TimeUnit.SECONDS;
			}else if(unit.equals(TimeUnit.MINUTES.name())){
				return TimeUnit.MINUTES;
			}else if(unit.equals(TimeUnit.HOURS.name())){
				return TimeUnit.HOURS;
			}else{
				return null;
			}
		}
	}
}
