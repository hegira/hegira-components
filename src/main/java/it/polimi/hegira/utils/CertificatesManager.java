/**
 * 
 */
package it.polimi.hegira.utils;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class CertificatesManager {
	private transient static Logger log = LoggerFactory.getLogger(CertificatesManager.class);
	private static String defaultPath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"components"+File.separator;
	private static String _datastore = "datastoreKey.p12";
	
	public static String getPrivateKeyPath(ManagedCertificates name, String certificateName){
		switch(name){
			case DATASTORE:
				if(certificateName!=null)
					return defaultPath+certificateName;
				else
					return defaultPath+_datastore;
			default:
				log.error("{} is not managed, i.e., a certificate/private key for {} doesn't exist!",
						name, name);
				return null;
		}
	}
	
	public enum ManagedCertificates {
		DATASTORE
	}
}
