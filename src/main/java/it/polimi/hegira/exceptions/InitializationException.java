package it.polimi.hegira.exceptions;

/**
 * @author Marco Scavuzzo
 *
 */
public class InitializationException extends Exception {

	private static final long serialVersionUID = 9042306310907526815L;

	public InitializationException(String string) {
		super(string);
	}

}
