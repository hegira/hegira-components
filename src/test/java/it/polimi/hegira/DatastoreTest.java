/**
 * 
 */
package it.polimi.hegira;

import static org.junit.Assert.*;

import java.util.List;

import it.polimi.hegira.adapters.datastore.Datastore;
import it.polimi.hegira.exceptions.ConnectException;

import org.junit.Test;

/**
 * @author Marco Scavuzzo
 *
 */
public class DatastoreTest {

	/**
	 * Test method for {@link it.polimi.hegira.adapters.datastore.Datastore#connect()}.
	 * @throws ConnectException 
	 */
	@Test
	public void testConnect() throws ConnectException {
		Datastore datastore = new Datastore(null);
		datastore.connect();
		List<String> kinds = datastore.getAllKinds();
		assertTrue(kinds.size()>0);
		for(String k:kinds)
			System.out.println(k);
	}

}
