/**
 * 
 */
package it.polimi.hegira.transformers;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import it.polimi.hegira.adapters.datastore.Datastore;
import it.polimi.hegira.models.AzureTablesModel;
import it.polimi.hegira.models.CassandraColumn;
import it.polimi.hegira.models.CassandraModel;
import it.polimi.hegira.models.DatastoreModel;
import it.polimi.hegira.models.Metamodel;
import it.polimi.hegira.transformers.TransformerFactory.Transformers;
import it.polimi.hegira.utils.Constants;
import it.polimi.hegira.utils.TestsUtils;

/**
 * @author Marco Scavuzzo
 *
 */
public class TransformationTests {
	private Datastore datastore;
	private DatastoreTransformer dst;
	private AzureTablesTransformer att;
	private CassandraTransformer ct;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.datastore = new Datastore(null);
		this.datastore.connect();
		
		this.dst = (DatastoreTransformer) TransformerFactory.getTransformer(Transformers.TDatastore);
		this.dst.setDatastoreService(this.datastore.getDatastoreService());
		this.dst.setTestMode(true);
		
		this.att = (AzureTablesTransformer) TransformerFactory.getTransformer(Transformers.TTables);
		this.ct = (CassandraTransformer) TransformerFactory.getTransformer(Transformers.TCassandra);
		this.ct.setConsistency(Constants.CONSISTENCY_EVENTUAL);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		datastore.disconnect();
	}

	
	@Test
	public void DStoATtest() throws IOException{
		Metamodel mm = TestsUtils.getMetamodelTestingEntity();
		
		DatastoreModel sourceEnt = dst.fromMyModel(mm);
		Metamodel intEnt = dst.toMyModel(sourceEnt);
		AzureTablesModel finalEnt = att.fromMyModel(intEnt);
		
		AzureTablesModel compEnt = att.fromMyModel(mm);
		
		assertEquals(compEnt, finalEnt);
	}
	
	@Test
	/**
	 * Azure Tables doesn't have the concept of column families. 
	 * Hence, when mapping data from Azure d.m. to the MDM, the table name is used as the only c.f.
	 * Therefore, the equivalence check should consider only the values of the keys and properties.
	 * Notice that in case of GC Datastore, the SDK doesn't allow to check directly only for the key value, but only for the whole Key. Since a GC Datastore key contains also the information regarding the Kind and the parent entity, the only thing we can check for is the Key name. 
	 * @throws IOException
	 */
	public void ATtoDStest() throws IOException{
		Metamodel mm = TestsUtils.getMetamodelTestingEntity();
		
		AzureTablesModel sourceEnt = att.fromMyModel(mm);
		Metamodel intEnt = att.toMyModel(sourceEnt);
		DatastoreModel finalEnt = dst.fromMyModel(intEnt);
		
		DatastoreModel compEnt = dst.fromMyModel(mm);
		
		assertEquals(compEnt, finalEnt);
	}
	
	@Test
	public void DStoCtest() throws IOException{
		Metamodel mm = TestsUtils.getMetamodelTestingEntity();
		
		DatastoreModel sourceEnt = dst.fromMyModel(mm);
		Metamodel intEnt = dst.toMyModel(sourceEnt);
		CassandraModel finalEnt = ct.fromMyModel(intEnt);
		
		CassandraModel compEnt = ct.fromMyModel(mm);
		
		assertEquals(compEnt, finalEnt);
	}
	
	
	public void CtoDStest() throws IOException{
		Metamodel mm = TestsUtils.getMetamodelTestingEntity();
		CassandraModel sourceEnt = ct.fromMyModel(mm);
		Metamodel intEnt = ct.toMyModel(sourceEnt);
		DatastoreModel finalEnt = dst.fromMyModel(intEnt);
		
		DatastoreModel compEnt = dst.fromMyModel(mm);
		
		assertEquals(compEnt, finalEnt);
	}
	
	@Test
	public void ATtoCtest() throws IOException{
		Metamodel mm = TestsUtils.getMetamodelTestingEntity();
		
		AzureTablesModel sourceEnt = att.fromMyModel(mm);
		Metamodel intEnt = att.toMyModel(sourceEnt);
		CassandraModel finalEnt = ct.fromMyModel(intEnt);
		
		CassandraModel compEnt = ct.fromMyModel(mm);
		
		//we already know that the column family and the indexes will be different because of Azure
		//and here we're not using additional meta-data that preserves this information.
		for(CassandraColumn c : finalEnt.getColumns()){
			c.setIndexed(true);
		}
		finalEnt.setTable("DoesntMatter");
		
		
		for(CassandraColumn c : compEnt.getColumns()){
			c.setIndexed(true);
		}
		compEnt.setTable("DoesntMatter");
		////
		
		assertEquals(compEnt, finalEnt);
	}
	
	@Test
	public void CtoATtest() throws IOException{
		Metamodel mm = TestsUtils.getMetamodelTestingEntity();
		//Convertendo dal MDM verso il Cassandra dm l'informazione sul partition group è mappata sulla consistenza 
		CassandraModel sourceEnt = ct.fromMyModel(mm);
		Metamodel intEnt = ct.toMyModel(sourceEnt);
		AzureTablesModel finalEnt = att.fromMyModel(intEnt);
		
		AzureTablesModel compEnt = att.fromMyModel(mm);
		
		assertEquals(compEnt, finalEnt);
	}
}
