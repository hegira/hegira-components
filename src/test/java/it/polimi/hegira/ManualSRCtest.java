/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.polimi.hegira;


import it.polimi.hegira.adapters.AbstractDatabase;
import it.polimi.hegira.adapters.DatabaseFactory;
import it.polimi.hegira.utils.Constants;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManualSRCtest {
	private static Logger log=LoggerFactory.getLogger(ManualSRCtest.class);

	public ManualSRCtest() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		log.debug("Received command message, destined to: SRC");
		HashMap<String, String> options_producer = new HashMap<String,String>();
		options_producer.put("mode", Constants.PRODUCER);
		
		options_producer.put("queue-address", "localhost");
		AbstractDatabase src = DatabaseFactory.getDatabase(Constants.CASSANDRA,
				options_producer);
		src.switchOver("SRC");
	}
}
